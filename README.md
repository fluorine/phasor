# Phasor
Phasor is a C++ framework for writing Wayland compositors. It is designed as an abstraction layer for low-level functionality, common in most desktop environments (modesetting, OpenGL contexts, input device configuration, etc.). Even though it is primarily designed to enable the development of the Fluorine desktop environment, its modular architecture should be able to accomodate the needs of different window compositors.

## Overview

The core idea behind Phasor is that the user creates a `phasor::Server`  class instance (or a subclass), and overrides the various components so as to customize the behaviour of the server, in a fashion quite similar to Mir. There are two types of behavior that can be customized:

* Interaction with the backend.
* Interaction with the frontend.

## Backends

A backend provides the server's I/O. It allows the compositor to configure and render to displays, and enables the window manager to configure and monitor input devices.

The most important backend is the Mesa backend. It is the default backend and it communicates directly with the GPU, providing access to the physical displays and input devices.

The other backend is the Qt backend. It can be configured to emulate different displays, which are realized as Qt windows. Needless to say, unlike "Mir on X", the Qt backend can also run within a Wayland or Mir compositor.

## Frontend

The frontend exposes a Wayland server for client applications to connect to. When a client communicates with the server, the events get forwarded to the corresponding subsystems that can be overriden by the custom compositor. The only frontend to be supported is Wayland since implementing a Mir frontend would require tremendous amount of work for questionable benefit. Nevertheless, no Wayland specifics should be visible in user code.

## Implementation details

Phasor is coded in C++14.

The build system is CMake. In Ubuntu systems, CMake can be installed with the `cmake` package.

Wherever possible, standard library features are used. Raw pointers should be avoided when a smart pointer type such as `std::shared_ptr` can be used instead.

## Dependencies

Below is a list of the dependencies necessary to build and use Phasor. The corresponding package for Ubuntu is enclosed in parentheses after the dependency.

* DRM development libraries - `libdrm-dev`
* Udev development files - `libudev-dev`
* GBM development files - `libgbm-dev`
* GLbinding - Build from sources (https://github.com/cginternals/glbinding)
* Wayland development libraries - `libwayland-dev`
* OpenGL development libraries - System dependent
* GLM - Build from sources (https://github.com/g-truc/glm)

For the Qt nested backend you also need to have Qt installed (`qtbase5-dev`).

