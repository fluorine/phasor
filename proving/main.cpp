#define USE_NESTED_PLATFORM

#include <iostream>
#include <phasor/server.h>
#include <phasor/display.h>

#ifndef USE_NESTED_PLATFORM
#include <phasor/mesa/platform.h>
#else
#include <phasor/nested/platform.h>
namespace pn = phasor::nested;
#endif

#include <phasor/compositor.h>
#include <phasor/mesa/gbm-buffer.h>
#include <phasor/frontend/wayland/surface.h>
#include <phasor/frontend/frontend.h>
#include <phasor/gl/program.h>
#include <phasor/gl/renderable.h>
#include <phasor/gl/simplerenderer.h>
#include <phasor/log.h>
#include <glbinding/Binding.h>
#include <glbinding/gl/gl.h>

namespace ph = phasor;
namespace pg = phasor::geometry;
namespace pgl = phasor::gl;
namespace pm = phasor::mesa;
namespace pfw = phasor::frontend::wayland;

pg::Length y_offset = pg::Length(0.0, pg::Length::inches);

namespace proving {

class Surface : public pfw::Surface {
public:
	int x, y, width, height;
	std::string title;

	virtual auto region() -> pg::Rectangle override {
		return pg::Rectangle(pg::Point {x, y},
		                     pg::Size {width, height});
	}
};

class Display : public phasor::Display {
public:
	Display(std::shared_ptr<ph::NativeDisplay> native_display,
	        int index) :
	    ph::Display::Display(native_display),
	    m_index(index)
	{
		m_top_offset = y_offset;
		m_left_offset = pg::Length(0.0, pg::Length::inches);
		y_offset = y_offset + native_display->height();
	}

	void initialize() override
	{
	}

	void create_gl_objects()
	{
		if (objects_created == true)
			return;
		m_renderer = std::make_shared<pgl::SimpleRenderer>(space().size);
		objects_created = true;
	}

	void render() override
	{
		create_gl_objects();

		gl::glClearColor(0.3f, 0.1f, 0.5f, 1.0f);
		gl::glClear(gl::GL_COLOR_BUFFER_BIT);

		m_renderer->start_frame();
		for (pfw::Surface *_s : ph::g_server->frontend().lock()->surfaces()) {
			if (_s->display().lock().get() != this)
				continue;
			m_renderer->render(_s);
			_s->frame_done();
			assert(::gl::glGetError() == 0);
		}
	}

protected:
	bool objects_created = false;
	std::shared_ptr<pgl::SimpleRenderer> m_renderer;
	std::shared_ptr<pm::GBMBuffer> m_cursor_buffer;
	int m_index;
};

class Compositor : public phasor::Compositor {
public:
	phasor::DisplayList configure_displays(
	        const phasor::NativeDisplayList& dpy_list) override
	{
		ph::DisplayList new_dpy_list;
		std::cout << "Found " << dpy_list.size() << " display(s)." << std::endl;
		int i = 0;
		for (std::shared_ptr<phasor::NativeDisplay> d : dpy_list) {
			std::cout << "Display: \"" << d->name() << "\"" << std::endl;
			std::cout
			        << "Width (inches): "
			        << d->width().as(phasor::geometry::Length::inches)
			        << std::endl;
			std::cout
			        << "Height (inches): "
			        << d->height().as(phasor::geometry::Length::inches)
			        << std::endl;
			for (phasor::DisplayMode mode : d->modes()) {
				std::cout << "Mode: " << mode.size << "@" << mode.refresh_rate
				          << std::endl;
			}
			d->set_active_mode(0); // Set the highest resolution.
			new_dpy_list.push_back(std::make_shared<Display>(d, i));
			i += 1;
		}
		m_current_display = new_dpy_list.back();
		m_pointer_position = pg::Point { 100, 100 };
		return new_dpy_list;
	}

	auto should_quit() -> bool override
	{
		return false;
	}

	auto create_surface() -> pfw::Surface* override
	{
		auto surface = new Surface();
		// Assign it to the first display.
		surface->set_display(m_display_list.back());
		static int last_pos = 10;
		surface->x = last_pos;
		surface->y = last_pos * 2;
		last_pos += 20;
		if (last_pos >= 200)
			last_pos = 10;
		return surface;
	}

	void set_surface_title(pfw::Surface *_surface,
	                       std::string title) override
	{
		auto surface = static_cast<Surface*>(_surface);
		surface->title = title;
	}

	void destroy_surface(pfw::Surface *s) override
	{
		std::string title = static_cast<Surface*>(s)->title;
		std::cout << "Destroying surface \"" <<
		             title << "\"" << std::endl;
	}
};

}

#ifndef USE_NESTED_PLATFORM
int main(int, char**)
#else
int main(int argc, char **argv)
#endif
{
	std::cout << "Phasor proving server." << std::endl;
	glbinding::Binding::initialize();

#ifdef USE_NESTED_PLATFORM
	pn::DisplayInfo test_dpy_info = { pg::Size { 1024, 768 },
	                                  "Primary Display",
	                                  100.0 };
	std::vector<pn::DisplayInfo> display_info;
	display_info.push_back(test_dpy_info);
#endif

	phasor::Server server(
            #ifdef USE_NESTED_PLATFORM
	            std::make_shared<phasor::nested::Platform>(&argc, &argv,
	                                                       display_info),
            #else
	            std::make_shared<phasor::mesa::Platform>(),
            #endif
	            std::make_shared<proving::Compositor>());
	    server.run();
	return 0;
}
