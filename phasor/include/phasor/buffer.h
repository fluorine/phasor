#ifndef PHASOR_BUFFER_H
#define PHASOR_BUFFER_H

#include <phasor/geometry/dimensions.h>
#include <phasor/geometry/size.h>

namespace phasor {

enum PixelFormat {
	phasor_pixel_format_invalid = 0,
	phasor_pixel_format_abgr_8888 = 1,
	phasor_pixel_format_xbgr_8888 = 2,
	phasor_pixel_format_argb_8888 = 3,
	phasor_pixel_format_xrgb_8888 = 4,
	phasor_pixel_format_bgr_888 = 5,
	phasor_pixel_format_rgb_888 = 6,
	phasor_pixel_formats = 7,
};

class Buffer {
public:
	virtual auto size() const -> geometry::Size = 0;
	virtual auto stride() const -> geometry::Stride = 0;
	virtual auto pixel_format() const -> PixelFormat = 0;
	virtual void begin_access() = 0;
	virtual void end_access() = 0;
	virtual auto data() -> void* = 0;
};

}

#endif // PHASOR_BUFFER_H
