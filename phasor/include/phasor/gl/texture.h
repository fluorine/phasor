#ifndef PHASOR_GL_TEXTURE_H
#define PHASOR_GL_TEXTURE_H

#include <glbinding/gl/types.h>
#include <glbinding/gl/enum.h>
#include <phasor/geometry/size.h>

namespace phasor {
namespace gl {

class Texture {
public:
	Texture();
	~Texture();

	inline auto id() -> ::gl::GLuint const { return m_id; }
	void bind();

	inline auto size() -> geometry::Size const { return m_size; }
	void set_data(geometry::Size size, void *data);

protected:
	auto generate_id() -> ::gl::GLuint;

private:
	::gl::GLuint m_id;
	geometry::Size m_size;
};

}
}

#endif // PHASOR_GL_TEXTURE_H

