#ifndef PHASOR_GL_SIMPLERENDERER_H
#define PHASOR_GL_SIMPLERENDERER_H

#include <phasor/gl/renderer.h>
#include <phasor/gl/program.h>
#include <phasor/geometry/size.h>
#include <memory>
#include <glm/mat4x4.hpp>

namespace phasor {
namespace gl {

class SimpleRenderer : public Renderer {
public:
	SimpleRenderer(geometry::Size display_size);
	virtual void start_frame();
	virtual void render(Renderable *r);

protected:
	std::shared_ptr<Program> m_program;
	glm::mat4x4 m_view_matrix;
	Program::Uniform m_contents_uniform;
	Program::Uniform m_matrix_uniform;
};

}
}

#endif // PHASOR_GL_SIMPLERENDERER_H

