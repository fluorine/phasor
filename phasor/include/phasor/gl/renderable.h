#ifndef PHASOR_GL_RENDERABLE_H
#define PHASOR_GL_RENDERABLE_H

#include <memory>
#include <phasor/geometry/rectangle.h>

namespace phasor {
namespace gl {

class Texture;

/**
 * @brief A texture that can be rendered on the screen.
 *
 * An object derived from the Renderable class can be passed to a Renderer
 * class to be rendered on the screen in an OpenGL context.
 */
class Renderable
{
public:
	/**
	 * @brief The contents of the renderable.
	 * @return A smart pointer to a texture that doesn't need to be bound.
	 */
	virtual auto texture() -> std::shared_ptr<Texture> = 0;

	/**
	 * @brief The region that the surface occupies on the screen.
	 * @return The region in device pixels.
	 */
	virtual auto region() -> geometry::Rectangle = 0;
};

}
}

#endif // PHASOR_GL_RENDERABLE_H
