#ifndef PHASOR_GL_RENDERER_H
#define PHASOR_GL_RENDERER_H

namespace phasor {
namespace gl {

class Renderable;

class Renderer {
public:
	virtual void start_frame() = 0;
	virtual void render(Renderable*) = 0;
};

}
}

#endif // PHASOR_GL_RENDERER_H

