#ifndef PHASOR_GL_PROGRAM_H
#define PHASOR_GL_PROGRAM_H

#include <glbinding/gl/types.h>
#include <glm/vec2.hpp>
#include <glm/mat2x2.hpp>
#include <glm/mat4x4.hpp>
#include <memory>

namespace phasor {
namespace gl {

class Texture;

/**
 * @brief Encapsulates an OpenGL program.
 *
 * This class lets you load and compile OpenGL shader programs quickly. It is
 * your responsibility to bind and unbind the program by calling Use() and
 * Release() respectively. Trying to set uniforms and attributes on an unbound
 * program is bound to result in trouble.
 */
class Program {
public:
	/**
	 * @brief Creates a new OpenGL progarm.
	 * @param vertex_src The source code of the vertex shader, exactly the way
	 *                     it should be compiled.
	 * @param fragment_src The source code of the fragment shader, exactly the
	 *                       way it should be compiled.
	 */
	Program(std::string vertex_src, std::string fragment_src);
	~Program();

	/**
	 * @brief Makes the program the currently bound OpenGL program.
	 *
	 * @note You don't need to release a previous program to bind another one.
	 */
	void use();

	/**
	 * @brief Unbinds this program.
	 */
	void release();

	/**
	 * @brief Represents an OpenGL uniform.
	 */
	class Uniform {
	public:
		Uniform(::gl::GLint id);
		Uniform();

		inline ::gl::GLint GetID() const {
			return m_id;
		}

		void set(float v) const;
		void set(double v) const;
		void set(int v) const;
		void set(std::shared_ptr<Texture> texture, int unit) const;
		void set(glm::vec2 v) const;
		void set(glm::dvec2 v) const;
		void set(glm::ivec2 v) const;
		void set(glm::mat2 m) const;
		void set(glm::mat4 m) const;

	private:
		::gl::GLint m_id;
	};

	/**
	 * @brief Returns a Uniform object corresponding to an OpenGL uniform.
	 * @param name The name of the uniform.
	 * @return A uniform object.
	 * @note The program does not need to be bound.
	 * An assertion failure will be thrown if the uniform is not found.
	 */
	Uniform find_uniform(std::string name) const;

protected:
	//! The OpenGL object ID of the program.
	::gl::GLint m_id;
};

}
}

#endif // PHASOR_GL_PROGRAM_H
