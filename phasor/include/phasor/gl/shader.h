#ifndef PHASOR_GL_SHADER_H
#define PHASOR_GL_SHADER_H

#include <glbinding/gl/enum.h>
#include <glbinding/gl/types.h>
#include <string>

namespace phasor {
namespace gl {

/**
 * @brief Encapsulates an OpenGL shader object.
 */
class Shader {
public:
    enum ShaderType {
		VERTEX = static_cast<unsigned int>(::gl::GL_VERTEX_SHADER),
		FRAGMENT = static_cast<unsigned int>(::gl::GL_FRAGMENT_SHADER),
		TESS_CONTROL = static_cast<unsigned int>(::gl::GL_TESS_CONTROL_SHADER),
		TESS_EVAL = static_cast<unsigned int>(::gl::GL_TESS_EVALUATION_SHADER),
		GEOMETRY = static_cast<unsigned int>(::gl::GL_GEOMETRY_SHADER),
    };

    /**
     * @brief Creates a new shader.
     * @param type The type of the shader to create.
     * @param source The complete source code of the shader.
     */
    Shader(ShaderType type, std::string source);
    ~Shader();

    /** Gets the underlying OpenGL ID. */
	inline ::gl::GLuint id() const {
        return m_ID;
    }

private:
	::gl::GLuint m_ID;
};

}
}

#endif // PHASOR_GL_SHADER_H
