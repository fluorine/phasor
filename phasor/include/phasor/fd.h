#ifndef PHASOR_FD_H
#define PHASOR_FD_H

namespace phasor {

class Fd {
public:
	Fd();
	Fd(int fd);
	~Fd();
	inline operator int() const { return m_fd; }
	inline auto is_valid() const -> bool { return m_fd != invalid; }
	void operator =(int fd);
	void close();

	static int const invalid = -1;
protected:
	int m_fd;
};

}

#endif // PHASOR_FD_H
