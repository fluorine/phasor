#ifndef PHASOR_GEOMETRY_POINT_H_
#define PHASOR_GEOMETRY_POINT_H_

#include "dimensions.h"
#include <iosfwd>

namespace phasor {
namespace geometry {

struct Point
{
    Point() = default;
    Point(Point const&) = default;
    Point& operator=(Point const&) = default;

    template<typename Xtype, typename Ytype>
    Point(Xtype x, Ytype y) : x(x), y(y) { }

    X x;
    Y y;
};

inline bool operator == (Point const& lhs, Point const& rhs)
{
    return (lhs.x == rhs.x) && (lhs.y == rhs.y);
}

inline bool operator != (Point const& lhs, Point const& rhs)
{
    return (lhs.x != rhs.x) || (lhs.y != rhs.y);
}

inline Point operator+(Point const& lhs, DeltaX const& rhs)
{
    return Point(lhs.x + rhs, lhs.y);
}

inline Point operator+(Point const &lhs, DeltaY const& rhs)
{
    return Point(lhs.x, lhs.y + rhs);
}

std::ostream& operator << (std::ostream& os, Point const& p);

}
}

#endif // PHASOR_GEOMETRY_POINT_H_
