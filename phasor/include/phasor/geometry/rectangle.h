#ifndef PHASOR_GEOMETRY_RECTANGLE_H_
#define PHASOR_GEOMETRY_RECTANGLE_H_

#include "point.h"
#include "size.h"
#include <iosfwd>

namespace phasor {
namespace geometry {

struct Rectangle
{
    Rectangle() = default;
    Rectangle(Point top_left, Size const& size) :
        top_left(top_left), size(size) { }

    Point top_left;
    Size size;

    Point bottom_left() const;
    Point bottom_right() const;
    Point top_right() const;

    bool contains(Point const& p) const;
    bool contains(Rectangle const& r) const;
    bool overlaps(Rectangle const& r) const;
    Rectangle intersection_with(Rectangle const& r) const;

    X left() const   { return top_left.x; }
    X right() const  { return bottom_right().x; }
    Y top() const    { return top_left.y; }
    Y bottom() const { return bottom_right().y; }
};

inline bool operator == (Rectangle const& lhs, Rectangle const& rhs)
{
    return lhs.top_left == rhs.top_left && lhs.size == rhs.size;
}

inline bool operator != (Rectangle const& lhs, Rectangle const& rhs)
{
    return lhs.top_left != rhs.top_left || rhs.size != rhs.size;
}

std::ostream& operator << (std::ostream& out, Rectangle const& rect);

}
}

#endif // PHASOR_GEOMETRY_RECTANGLE_H_
