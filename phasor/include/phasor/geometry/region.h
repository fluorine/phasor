#ifndef PHASOR_GEOMETRY_REGION_H
#define PHASOR_GEOMETRY_REGION_H

#include <phasor/geometry/rectangle.h>
#include <vector>

namespace phasor {
namespace geometry {

//!
//! \brief Represents a region on the screen.
//!
//! A region is the result of adding and removing rectangles, as specified in
//! the Wayland protocol. In this class, the rectangles that were added are
//! stored in the m_positive vector, while the ones that were subtracted in the
//! m_negative vector.
//!
//! Checking whether a point belongs to a region is a matter of checking if it
//! belongs to at least one of the "positive" rectangles, while not being inside
//! any of the "negative" ones. This might not be the most efficient
//! implementation but for the simple regions that most clients will create, it
//! is the best one in terms of complexity.
//!
class Region {
public:
	Region();
	void operator+=(const Rectangle &rhs);
	void operator-=(const Rectangle &rhs);

	auto contains(const Point &p) const -> bool;

	auto positives() -> std::vector<Rectangle>& { return m_positives; }
	auto negatives() -> std::vector<Rectangle>& { return m_negatives; }

protected:
	std::vector<Rectangle> m_positives;
	std::vector<Rectangle> m_negatives;
};

}
}

#endif // PHASOR_GEOMETRY_REGION_H


