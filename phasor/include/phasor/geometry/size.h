#ifndef PHASOR_GEOMETRY_SIZE_H_
#define PHASOR_GEOMETRY_SIZE_H_

#include "dimensions.h"

namespace phasor {
namespace geometry {

struct Size
{
    Size() { }
    Size(Size const&) = default;
    Size& operator=(Size const&) = default;

    template<typename WidthType, typename HeightType>
    Size(WidthType&& width, HeightType&& height) : width(width), height(height) { }

    Width width;
    Height height;
};

inline bool operator == (Size const &lhs, Size const& rhs)
{
    return lhs.width == rhs.width && lhs.height == rhs.height;
}

inline bool operator != (Size const &lhs, Size const& rhs)
{
    return lhs.width != rhs.width || lhs.height != rhs.height;
}

template<typename Scalar>
inline Size operator * (Scalar scale, Size const& size)
{
    return Size(scale * size.width, scale * size.height);
}

template<typename Scalar>
inline Size operator * (Size const &size, Scalar scale)
{
    return scale * size;
}

std::ostream& operator << (std::ostream& out, Size const &size);

#ifdef PHASOR_GEOMETRY_DISPLACEMENT_H_
inline Displacement as_displacement(Size const& size)
{
    return Displacement(size.width.as_int(), size.height.as_int());
}

inline Size as_size(Displacement const& disp)
{
    return Size(disp.dx.as_int(), disp.dy.as_int());
}
#endif

}
}

#endif // PHASOR_GEOMETRY_SIZE_H_
