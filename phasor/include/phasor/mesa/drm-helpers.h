#ifndef PHASOR_MESA_DRM_HELPERS_H
#define PHASOR_MESA_DRM_HELPERS_H

#include <string>

namespace phasor {
namespace mesa {

class DRMHelpers {
protected:
	auto find_primary_gpu() const -> std::string;
};

}
}

#endif // PHASOR_MESA_DRM_HELPERS_H
