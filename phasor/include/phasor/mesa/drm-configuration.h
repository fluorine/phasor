#ifndef PHASOR_DRM_CONFIGURATION_H
#define PHASOR_DRM_CONFIGURATION_H

#include <phasor/fd.h>
#include <phasor/display.h>
#include <phasor/mesa/drm-helpers.h>
#include <gbm.h>
#include <EGL/egl.h>
#include <memory>

namespace phasor {
namespace mesa {

class DRMModeResources;
class Platform;
class DRMConfiguration : protected DRMHelpers {
public:
	DRMConfiguration();
	~DRMConfiguration();

	auto scan_displays() -> phasor::NativeDisplayList;
	void apply_display_configuration(const phasor::DisplayList &list);
	void update_displays();

private:
	friend class phasor::mesa::Platform;
	Fd m_fd;
	std::shared_ptr<DRMModeResources> m_drm_resources;
	struct gbm_device *m_gbm_device;
	EGLDisplay m_egl_display;
	EGLConfig m_egl_config;
	EGLContext m_egl_ctx;

public:
	void update_display(std::shared_ptr<phasor::Display> display, void *data);
};

}
}

#endif // PHASOR_DRM_CONFIGURATION_H
