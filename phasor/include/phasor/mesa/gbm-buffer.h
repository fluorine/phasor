#ifndef PHASOR_MESA_GBM_BUFFER_H

#include <phasor/buffer.h>
#include <phasor/geometry/size.h>
#include <gbm.h>

namespace phasor {
namespace mesa {

class DRMConfiguration;

extern struct gbm_device *s_gbm_device;

class GBMBuffer : public phasor::Buffer {
public:
	GBMBuffer(geometry::Size size, phasor::PixelFormat format, uint32_t flags);
	~GBMBuffer();

	auto size() const -> geometry::Size override;
	auto stride() const -> geometry::Stride override;
	auto pixel_format() const -> phasor::PixelFormat override;
	void begin_access() override;
	void end_access() override;
	auto data() -> void* override;
	auto handle() -> gbm_bo_handle;

protected:
	struct gbm_bo *m_bo;
	void *m_data;
	geometry::Size m_size;
	geometry::Stride m_stride;
	PixelFormat m_format;
};

}
}

#endif // PHASOR_MESA_GBM_BUFFER_H
