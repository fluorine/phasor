#ifndef PHASOR_MESA_INPUT_H
#define PHASOR_MESA_INPUT_H

#include <memory>

struct libinput;
struct libinput_event;

namespace phasor {
class Compositor;

namespace mesa {
namespace udev { class Context; }

class Input {
public:
	Input();
	virtual ~Input();
	void update_input(std::shared_ptr<Compositor> compositor);

protected:
	std::shared_ptr<udev::Context> m_udev;
	struct libinput *m_libinput;

	void process_event(std::shared_ptr<Compositor> compositor,
	                   struct libinput_event *e);
};

}
}

#endif // PHASOR_MESA_INPUT_H
