#ifndef PHASOR_MESA_NATIVE_DISPLAY_H
#define PHASOR_MESA_NATIVE_DISPLAY_H

#include <phasor/display.h>
#include <phasor/mesa/drm-mode-resources.h>
#include <gbm.h>
#include <EGL/egl.h>

namespace phasor {
namespace mesa {

class GBMBuffer;

class NativeDisplay : public phasor::NativeDisplay {
public:
	NativeDisplay(std::vector<DisplayMode> modes, int active_mode,
	              std::string name,
	              geometry::Length width,
	              geometry::Length height,
	              uint32_t connector_id);
	auto modes() const -> std::vector<DisplayMode> override;
	void set_active_mode(int index) override;
	auto get_active_mode() const -> int override;
	auto name() const -> std::string override;
	auto width() const -> geometry::Length override;
	auto height() const -> geometry::Length override;
	void set_cursor(std::shared_ptr<Buffer> buffer) override;
	void move_cursor(geometry::Point new_position) override;
	void hide_cursor() override;
	void show_cursor() override;

private:
	friend class DRMConfiguration;
	std::vector<DisplayMode> m_modes;
	int m_active_mode;
	std::string m_name;
	geometry::Length m_width;
	geometry::Length m_height;
	uint32_t m_connector_id;

	/// @todo Wrap the following in classes.
public: // Used by page_flip_handler()
	struct gbm_surface *m_gbm_sfc;
	EGLSurface m_egl_sfc;
	struct gbm_bo *m_bo;
	struct gbm_bo *m_next_bo;
	uint32_t m_fb_id;
	uint32_t m_crtc_id;
	int m_fd;
	std::shared_ptr<GBMBuffer> m_cursor_buffer;
};

}
}

#endif // PHASOR_MESA_NATIVE_DISPLAY_H
