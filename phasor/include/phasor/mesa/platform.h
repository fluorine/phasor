#ifndef PHASOR_MESA_PLATFORM_H
#define PHASOR_MESA_PLATFORM_H

#include <phasor/platform.h>
#include <gbm.h>
#include <memory>

namespace phasor {
namespace mesa {

class DRMConfiguration;
class Input;

class Platform : public phasor::Platform {
public:
	Platform();
	~Platform();
	NativeDisplayList scan_displays() override;
	void apply_display_configuration(const DisplayList &list) override;
	void update_displays() override;
	void update_input(std::shared_ptr<Compositor> compositor) override;

	// Helper functions
	auto gbm() -> struct gbm_device*;
protected:
	std::shared_ptr<DRMConfiguration> m_drm;
	std::shared_ptr<Input> m_input;
};

}
}

#endif // PHASOR_MESA_PLATFORM_H
