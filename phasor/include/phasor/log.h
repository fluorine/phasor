#ifndef PHASOR_LOG_H
#define PHASOR_LOG_H

#include <string>

namespace phasor {

enum Severity {
	DEBUG = 0,
	INFO = 1,
	WARNING = 2,
	ERROR = 3,
};

void log(Severity severity, std::string message, std::string component = "global");

}

#endif // PHASOR_LOG_H
