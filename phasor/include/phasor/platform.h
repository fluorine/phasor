#ifndef PHASOR_BACKEND_H
#define PHASOR_BACKEND_H

#include <phasor/display.h>

namespace phasor {

class Compositor;
class Platform
{
public:
	virtual NativeDisplayList scan_displays() = 0;
	virtual void apply_display_configuration(const DisplayList& dpy_list) = 0;
	/// Repaints every display, calling the compositor-provided Display class.
	/// May block to synchronize page flips.
	virtual void update_displays() = 0;
	virtual void update_input(std::shared_ptr<Compositor> compositor) = 0;
};

}

#endif // PHASOR_BACKEND_H
