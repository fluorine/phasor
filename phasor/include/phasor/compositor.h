#ifndef PHASOR_COMPOSITOR_H
#define PHASOR_COMPOSITOR_H

#include <phasor/display.h>
#include <phasor/input.h>
#include <phasor/geometry/point.h>

namespace phasor {

namespace frontend {
namespace wayland {
class Surface;
}
}

class Compositor
{
public:
	virtual DisplayList configure_displays(
	        const NativeDisplayList &native_displays) = 0;
	virtual auto should_quit() -> bool = 0;
	virtual auto create_surface() -> frontend::wayland::Surface* = 0;
	virtual void set_surface_title(frontend::wayland::Surface*,
	                               std::string title) = 0;
	virtual void destroy_surface(frontend::wayland::Surface*) = 0;

	// Input handlers
	virtual void handle_pointer_motion(input::PointerMotion *e);
	virtual void handle_pointer_position(
	        phasor::geometry::Point new_pos,
	        std::shared_ptr<phasor::Display> display);

protected:
	friend class Server;
	DisplayList m_display_list;

	// Default mouse pointer implementation.
	geometry::Point m_pointer_position; // Relative to the top-left corner
	                                    // of the current display.
	std::weak_ptr<Display> m_current_display;

	auto next_display_right(geometry::Point &pos) -> std::shared_ptr<Display>;
	auto next_display_left(geometry::Point &pos) -> std::shared_ptr<Display>;
	auto next_display_above(geometry::Point &pos) -> std::shared_ptr<Display>;
	auto next_display_below(geometry::Point &pos) -> std::shared_ptr<Display>;
};

}

#endif // PHASOR_COMPOSITOR_H
