#ifndef PHASOR_SERVER_H
#define PHASOR_SERVER_H

#include <memory>

namespace phasor {

class Platform;
class Compositor;
namespace frontend {
    class Frontend;
}

class Server {
public:
	Server(std::shared_ptr<Platform> platform,
	       std::shared_ptr<Compositor> compositor);
	int run();

	inline auto compositor() -> std::weak_ptr<Compositor> {
		return m_compositor;
	}

	inline auto platform() -> std::weak_ptr<Platform> {
		return m_platform;
	}

	inline auto frontend() -> std::weak_ptr<frontend::Frontend> {
		return m_frontend;
	}

protected:
	std::shared_ptr<Platform> m_platform;
	std::shared_ptr<Compositor> m_compositor;
	std::shared_ptr<frontend::Frontend> m_frontend;
};

// To be used by the Wayland frontend.
// May not be the prettiest design but the structure
// of libwayland mandates this.
extern Server *g_server;

}

#endif // PHASOR_SERVER_H
