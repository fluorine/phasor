#ifndef PHASOR_DISPLAY_H
#define PHASOR_DISPLAY_H

#include <string>
#include <vector>
#include <memory>
#include <phasor/geometry/length.h>
#include <phasor/geometry/rectangle.h>
#include <phasor/buffer.h>

namespace phasor {

struct DisplayMode {
	geometry::Size size;
	int refresh_rate;
};

class NativeDisplay {
public:
	virtual std::vector<DisplayMode> modes() const = 0;

	/// @param index Index or disabled to turn the display off.
	virtual void set_active_mode(int index) = 0;
	virtual auto get_active_mode() const -> int = 0;
	virtual auto name() const -> std::string = 0;
	virtual auto width() const -> geometry::Length = 0;
	virtual auto height() const -> geometry::Length = 0;
	virtual void set_cursor(std::shared_ptr<Buffer> buffer) = 0;
	virtual void move_cursor(geometry::Point new_position) = 0;
	virtual void hide_cursor() = 0;
	virtual void show_cursor() = 0;

	const static int disabled = -1;
};

class Display
{
public:
	Display(std::shared_ptr<NativeDisplay> native_display);

	inline float dpi_horizontal() const { return m_h_dpi; }
	inline float dpi_vertical() const { return m_v_dpi; }

	inline std::string name() const { return m_name; }
	inline geometry::Rectangle space() const { return m_space; }
	inline geometry::Length width() const { return m_width; }
	inline geometry::Length height() const { return m_height; }
	inline geometry::Length left_offset() const { return m_left_offset; }
	inline geometry::Length top_offset() const { return m_top_offset; }

	inline std::shared_ptr<NativeDisplay> native_display() {
		return m_native_display;
	}

	virtual void initialize() = 0;
	virtual void render() = 0;

protected:
	std::shared_ptr<NativeDisplay> m_native_display;
	std::string m_name;
	geometry::Rectangle m_space;
	geometry::Length m_width;
	geometry::Length m_height;
	geometry::Length m_left_offset;
	geometry::Length m_top_offset;
	float m_h_dpi;
	float m_v_dpi;

	void update_metrics();
};

typedef std::vector<std::shared_ptr<NativeDisplay>> NativeDisplayList;
typedef std::vector<std::shared_ptr<Display>> DisplayList;

}

#endif // PHASOR_DISPLAY_H
