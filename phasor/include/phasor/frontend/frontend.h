#ifndef PHASOR_FRONTEND_H
#define PHASOR_FRONTEND_H

#include <memory>
#include <vector>
#include <list>
#include <wayland-server.h>

namespace phasor {
namespace geometry {
class Region;
}

namespace frontend {

namespace wayland {
class Display;
class Surface;
class Client;
}

class Frontend {
public:
	Frontend();
	~Frontend();

	void start();
	void stop();

	void update();

	inline auto surfaces() -> std::list<wayland::Surface*>& {
		return m_surfaces;
	}

protected:
	std::shared_ptr<wayland::Display> m_display;
	std::list<wayland::Surface*> m_surfaces;
	std::vector<std::shared_ptr<wayland::Client>> m_clients;

public:
	// Wayland request handlers.
	void wl_create_surface(wl_client *client, wl_resource *_surface);
	void xdg_shell_get_surface(wl_client *, wl_resource *_xdg_surface,
	                           wl_resource *_wl_surface);
	void xdg_surface_set_title(wl_client *, wl_resource *_surface,
	                           const char *title);

	void wl_surface_attach(wayland::Surface * s, wl_resource *buffer);
	void wl_surface_commit(wayland::Surface * s);
	void wl_surface_frame(wayland::Surface *s, wl_resource *callback);
	void wl_surface_set_input_region(wayland::Surface *s,
	                                 phasor::geometry::Region *region);
	void wl_surface_set_opaque_region(wayland::Surface *s,
	                                  phasor::geometry::Region *region);
	void wl_surface_set_buffer_scale(wayland::Surface *s, int scale);
	void wl_client_destroyed(wl_client *raw_client);

	// Helpers
	auto find_client(wl_client *wc) -> std::shared_ptr<wayland::Client>;
};

}
}

#endif // PHASOR_FRONTEND_H
