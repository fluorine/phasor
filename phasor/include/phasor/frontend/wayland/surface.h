#ifndef PHASOR_FRONTEND_SURFACE_H
#define PHASOR_FRONTEND_SURFACE_H

#include <wayland-server.h>
#include <phasor/geometry/size.h>
#include <phasor/geometry/region.h>
#include <phasor/gl/renderable.h>
#include <memory>

namespace phasor {

class Display;

namespace gl { class Texture; }
namespace frontend {
namespace wayland {

class Client;
class Surface : public gl::Renderable {
public:
	virtual ~Surface();
	auto wl_surface() -> wl_resource* const { return m_wl_surface; }
	auto xdg_surface() -> wl_resource* const { return m_xdg_surface; }
	auto wl_buffer() -> wl_resource* const { return m_wl_buffer; }
	auto wl_frame_callback() -> wl_resource *const {
		return m_wl_frame_callback;
	}
	auto client() -> std::weak_ptr<Client> { return m_client; }
	auto display() -> std::weak_ptr<Display> { return m_display; }
	virtual auto texture() -> std::shared_ptr<gl::Texture> { return m_texture; }
	auto is_damaged() -> bool const { return m_is_damaged; }
	auto input_region() -> phasor::geometry::Region const {
		return m_input_region;
	}
	auto opaque_region() -> phasor::geometry::Region const {
		return m_opaque_region;
	}
	auto buffer_scale() -> int const { return m_buffer_scale; }

	void set_wl_surface(wl_resource *r) {
		m_wl_surface = r;
	}

	void set_xdg_surface(wl_resource *r) {
		m_xdg_surface = r;
	}

	void set_wl_buffer(wl_resource *r) {
		m_wl_buffer = r;
	}

	void set_wl_frame_callback(wl_resource *r) {
		m_wl_frame_callback = r;
	}

	void set_client(std::weak_ptr<Client> c) {
		m_client = c;
	}

	void set_display(std::weak_ptr<Display> d) {
		m_display = d;
	}

	void set_damaged(bool damaged = true) {
		m_is_damaged = damaged;
	}

	void set_input_region(phasor::geometry::Region r) {
		m_input_region = r;
	}

	void set_opaque_region(phasor::geometry::Region r) {
		m_opaque_region = r;
	}

	void set_buffer_scale(int scale) {
		m_buffer_scale = scale;
	}

	void commit();
	void frame_done();
	void send_configure(phasor::geometry::Size size);

private:
	wl_resource *m_wl_surface = nullptr;
	wl_resource *m_xdg_surface = nullptr;
	wl_resource *m_wl_buffer = nullptr;
	wl_resource *m_wl_frame_callback = nullptr;

	std::weak_ptr<Client> m_client;
	std::weak_ptr<Display> m_display;
	std::shared_ptr<gl::Texture> m_texture;

	bool m_is_damaged = false;
	phasor::geometry::Region m_input_region;
	phasor::geometry::Region m_opaque_region;
	int m_buffer_scale;
};

}
}
}

#endif // PHASOR_FRONTEND_SURFACE_H
