#ifndef PHASOR_FRONTEND_WAYLAND_DISPLAY_H
#define PHASOR_FRONTEND_WAYLAND_DISPLAY_H

#include <wayland-server.h>

namespace phasor {
namespace frontend {
namespace wayland {

class Display {
public:
	Display();
	~Display();

	void dispatch_events();
protected:
	wl_display *m_display;
	wl_event_loop *m_event_loop;
};

}
}
}

#endif // PHASOR_FRONTEND_WAYLAND_DISPLAY_H
