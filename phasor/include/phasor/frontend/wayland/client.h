#ifndef PHASOR_FRONTEND_WAYLAND_CLIENT_H
#define PHASOR_FRONTEND_WAYLAND_CLIENT_H

#include <wayland-server.h>

namespace phasor {
namespace frontend {
namespace wayland {

class Client {
public:
	Client(wl_client *client);

	auto _wl_client() -> struct wl_client* const { return m_wl_client; }
	void set_wl_client(struct wl_client *r) {
		m_wl_client = r;
	}

	auto wl_destroy_listener() -> struct wl_listener* {
		return &m_destroy_listener;
	}

	void ping();

protected:
	struct wl_client *m_wl_client;

	// This needs to be public so that we can use Wayland's wl_container_of()
public:
	struct wl_listener m_destroy_listener;
	/// @todo add wl_keyboard and wl_pointer
};

}
}
}

#endif // PHASOR_FRONTEND_WAYLAND_CLIENT_H
