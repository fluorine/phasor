#ifndef PHASOR_NESTED_NATIVE_DISPLAY_H
#define PHASOR_NESTED_NATIVE_DISPLAY_H

#include <phasor/display.h>

namespace phasor {
namespace nested {

class NestedSurface;

class NativeDisplay : public phasor::NativeDisplay
{
public:
	NativeDisplay(geometry::Size size, std::string name, float dpi);

	auto modes() const -> std::vector<DisplayMode> override;
	void set_active_mode(int index) override;
	auto get_active_mode() const -> int override;
	auto name() const -> std::string override;
	auto width() const -> geometry::Length override;
	auto height() const -> geometry::Length override;
	void set_cursor(std::shared_ptr<Buffer> buffer) override;
	void move_cursor(geometry::Point new_position) override;
	void hide_cursor() override;
	void show_cursor() override;

	void show_nested();
	void destroy_nested();
	void repaint();

	auto nested_surface() -> std::shared_ptr<NestedSurface> {
		return m_surface;
	}

protected:
	geometry::Size m_size;
	geometry::Length m_width;
	geometry::Length m_height;
	std::string m_name;
	bool m_active;
	bool m_autoAdjustDPI;

	friend class Platform;
	std::shared_ptr<NestedSurface> m_surface;
	std::weak_ptr<Display> m_display;
};

}
}

#endif // PHASOR_NESTED_NATIVE_DISPLAY_H
