#ifndef PHASOR_NESTED_PLATFORM_H
#define PHASOR_NESTED_PLATFORM_H

#include <phasor/platform.h>
#include <phasor/nested/native-display.h>

class QGuiApplication;

namespace phasor {
namespace nested {

struct DisplayInfo {
	geometry::Size size;
	std::string name;
	float dpi;
};

class Platform : public phasor::Platform {
public:
	Platform(int *argc, char ***argv, std::vector<DisplayInfo> displays);
	~Platform();
	auto scan_displays() -> phasor::NativeDisplayList override;
	void apply_display_configuration(const DisplayList& dpy_list) override;
	void update_displays() override;
	void update_input(std::shared_ptr<Compositor> compositor) override;

protected:
	QGuiApplication* m_qt_app;
	std::vector<std::shared_ptr<NativeDisplay>> m_native_displays;
	DisplayList m_displays;
};

}
}

#endif // PHASOR_NESTED_PLATFORM_H
