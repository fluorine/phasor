#ifndef PHASOR_NESTED_NESTED_SURFACE_H
#define PHASOR_NESTED_NESTED_SURFACE_H

#include <QOpenGLWindow>
#include <memory>

namespace phasor {
class Display;

namespace nested {

class NestedSurface : public QOpenGLWindow
{
	Q_OBJECT
public:
	NestedSurface();
	~NestedSurface();
	void get_mouse_delta(int &dx, int &dy);

protected:
	void paintGL() override;
	void mouseMoveEvent(QMouseEvent *e) override;

	int last_mouse_X = -1;
	int last_mouse_Y = -1;
	int mouse_dx = 0;
	int mouse_dy = 0;

	friend class NativeDisplay;
	std::weak_ptr<phasor::Display> m_display;
};

}
}

#endif // PHASOR_NESTED_NESTED_SURFACE_H
