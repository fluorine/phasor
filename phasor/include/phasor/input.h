#ifndef PHASOR_INPUT_H
#define PHASOR_INPUT_H

#include <inttypes.h>

namespace phasor {
namespace input {

struct Event {
	uint32_t time;
	uint32_t time_usec;
};

struct PointerMotion : public Event {
	double dx;
	double dy;
};

}

}

#endif // PHASOR_INPUT_H
