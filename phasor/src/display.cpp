#include <phasor/display.h>

namespace ph = phasor;
namespace pg = phasor::geometry;

ph::Display::Display(std::shared_ptr<NativeDisplay> native_display) :
    m_native_display(native_display)
{
	update_metrics();
}

void ph::Display::update_metrics()
{
	m_name = m_native_display->name();
	ph::DisplayMode active_mode = m_native_display->modes()
	        .at(m_native_display->get_active_mode());
	m_width = m_native_display->width();
	m_height = m_native_display->height();
	m_h_dpi = active_mode.size.width.as_int() / m_width.as(pg::Length::inches);
	m_v_dpi = active_mode.size.height.as_int() / m_height.as(pg::Length::inches);
	m_space = pg::Rectangle { {0, 0},
	        {active_mode.size.width.as_int(),
	        active_mode.size.height.as_int() } };
}
