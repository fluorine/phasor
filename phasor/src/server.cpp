#include <phasor/server.h>
#include <phasor/platform.h>
#include <phasor/compositor.h>
#include <phasor/frontend/frontend.h>
#include <iostream>

namespace ph = phasor;

ph::Server *ph::g_server;

ph::Server::Server(std::shared_ptr<Platform> platform,
                   std::shared_ptr<Compositor> compositor) :
    m_platform(platform),
    m_compositor(compositor),
    m_frontend(new ph::frontend::Frontend())
{
	// This means that no client can use two servers simultaneously,
	// not a big issue for many, I suppose ;)
	ph::g_server = this;
}

int ph::Server::run()
{
	phasor::NativeDisplayList native_dpy_list = m_platform->scan_displays();
	m_compositor->m_display_list =
	        m_compositor->configure_displays(native_dpy_list);
	m_platform->apply_display_configuration(m_compositor->m_display_list);
	for (std::shared_ptr<ph::Display> dpy : m_compositor->m_display_list) {
		dpy->initialize();
	}

	m_frontend->start();
	while (m_compositor->should_quit() == false) {
		m_frontend->update();
		m_platform->update_input(m_compositor);
		m_platform->update_displays();
	}
	m_frontend->stop();

	return 0;
}
