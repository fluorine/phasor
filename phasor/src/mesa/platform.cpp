#include <phasor/mesa/platform.h>
#include <phasor/mesa/drm-configuration.h>
#include <phasor/mesa/input.h>
#include <iostream>
#include <glbinding/Binding.h>

namespace ph = phasor;
namespace pm = phasor::mesa;

pm::Platform::Platform() :
    m_drm(std::make_shared<pm::DRMConfiguration>()),
    m_input(std::make_shared<pm::Input>())
{
	// This should be done by the user anyway, but just to be sure, and to
	// let the user perhaps use another GL function loader.
	glbinding::Binding::initialize();
}

pm::Platform::~Platform()
{
}

ph::NativeDisplayList pm::Platform::scan_displays()
{
	return m_drm->scan_displays();
}

void pm::Platform::apply_display_configuration(const DisplayList &list)
{
	m_drm->apply_display_configuration(list);
}

void pm::Platform::update_displays()
{
	m_drm->update_displays();
}

void pm::Platform::update_input(std::shared_ptr<Compositor> compositor)
{
	m_input->update_input(compositor);
}

