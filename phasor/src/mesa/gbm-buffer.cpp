#include <phasor/mesa/gbm-buffer.h>
#include <stdexcept>

namespace ph = phasor;
namespace pm = phasor::mesa;
namespace pg = phasor::geometry;

namespace phasor {
namespace mesa {
    struct gbm_device *s_gbm_device = nullptr;
}
}

static uint32_t phasor_to_gbm_format(ph::PixelFormat format)
{
	switch (format) {
	case ph::phasor_pixel_format_argb_8888:
		return GBM_FORMAT_ARGB8888;
	case ph::phasor_pixel_format_xrgb_8888:
		return GBM_FORMAT_XRGB8888;
	default:
		throw std::logic_error("GBM only supports ARGB and XRGB formats.");
	}
}

pm::GBMBuffer::GBMBuffer(geometry::Size size,
                         phasor::PixelFormat format,
                         uint32_t flags) :
    m_data(nullptr),
    m_size(size),
    m_format(format)
{
	uint32_t gbm_format = phasor_to_gbm_format(format);
	m_stride = size.width.as_int() * 4;
	if (s_gbm_device == nullptr)
		throw std::logic_error("GBM device not set.");
	m_bo = gbm_bo_create(s_gbm_device,
	                     (uint32_t)size.width.as_int(),
	                     (uint32_t)size.height.as_int(),
	                     gbm_format,
	                     flags);
	if (m_bo == nullptr)
		throw std::runtime_error("GBM buffer creation failed.");
}

pm::GBMBuffer::~GBMBuffer()
{
	gbm_bo_destroy(m_bo);
}


auto pm::GBMBuffer::size() const -> pg::Size
{
	return m_size;
}

auto pm::GBMBuffer::stride() const -> pg::Stride
{
	return m_stride;
}

auto pm::GBMBuffer::pixel_format() const -> phasor::PixelFormat
{
	return m_format;
}

void pm::GBMBuffer::begin_access()
{
	if (m_data != nullptr)
		throw std::runtime_error("Attempting to map GBM buffer twice.");
#if 1
	uint32_t stride = 0;
	m_data = gbm_bo_map(m_bo, 0, 0,
	                    (uint32_t)m_size.width.as_int(),
	                    (uint32_t)m_size.height.as_int(),
	                    GBM_BO_TRANSFER_READ_WRITE,
	                    &stride, &m_data);
	m_stride = stride;
#else
	m_data = calloc(4, m_size.width.as_int() * m_size.height.as_int());
#endif
}

void pm::GBMBuffer::end_access()
{
	if (m_data == nullptr)
		throw std::runtime_error("Attempting to unmap GBM buffer twice.");
#if 1
	gbm_bo_unmap(m_bo, m_data);
#else
	gbm_bo_write(m_bo, m_data, 4 * m_size.width.as_int() * m_size.height.as_int());
	free(m_data);
#endif
	m_data = nullptr;
}

auto pm::GBMBuffer::data() -> void*
{
	return m_data;
}

auto pm::GBMBuffer::handle() -> gbm_bo_handle
{
	return gbm_bo_get_handle(m_bo);
}
