#include <phasor/mesa/drm-configuration.h>
#include <phasor/mesa/drm-mode-resources.h>
#include <phasor/mesa/native-display.h>
#include <phasor/mesa/drm-connector.h>
#include <phasor/mesa/gbm-buffer.h>
#include <unistd.h>
#include <fcntl.h>
#include <xf86drm.h>
#include <xf86drmMode.h>
#include <iostream>
#include <errno.h>
#include <cstring>
#include <glbinding/gl/gl.h>
namespace ph = phasor;
namespace pg = phasor::geometry;
namespace pm = phasor::mesa;

using namespace gl;

pm::DRMConfiguration::DRMConfiguration()
{
	std::string primary_gpu = find_primary_gpu();
	m_fd = open(primary_gpu.c_str(), O_RDWR | O_CLOEXEC);
	if (!m_fd.is_valid())
		throw std::runtime_error("Failed to open the GPU device node.");

	int error = drmSetMaster(m_fd);
	if (error != 0) {
		// There's no problem modesetting even without being the drm master.
		std::cout << "Warning: Failed to set the DRM master - " <<
		             std::strerror(errno) << std::endl;
	}

	m_gbm_device = gbm_create_device(m_fd);
	if (m_gbm_device == nullptr)
		throw std::runtime_error("Failed to create  GBM device.");
	phasor::mesa::s_gbm_device = m_gbm_device;

	m_egl_display = eglGetDisplay(m_gbm_device);
	EGLint major, minor;
	if (eglInitialize(m_egl_display, &major, &minor) == false)
		throw std::runtime_error("Failed to initialize EGL.");

	if (eglBindAPI(EGL_OPENGL_ES_API) == false)
		throw std::runtime_error("Failed to bind the OpenGL ES API.");

	// OpenGL ES v3.0
	static const EGLint context_attribs[] = {
	    EGL_CONTEXT_CLIENT_VERSION, 3,
	    EGL_NONE
	};

	static const EGLint config_attribs[] = {
	    EGL_SURFACE_TYPE, EGL_WINDOW_BIT,
	    EGL_RED_SIZE, 1,
	    EGL_GREEN_SIZE, 1,
	    EGL_BLUE_SIZE, 1,
	    EGL_ALPHA_SIZE, 0,
	    EGL_RENDERABLE_TYPE, EGL_OPENGL_ES_BIT,
	    EGL_NONE
	};

	EGLint n_configs = 0;
	if ((eglChooseConfig(m_egl_display, config_attribs, &m_egl_config,
	                     1, &n_configs) == false) || n_configs != 1)
		throw std::runtime_error("Failed to choose an EGL configuration.");

	m_egl_ctx = eglCreateContext(m_egl_display,
	                             m_egl_config,
	                             EGL_NO_CONTEXT,
	                             context_attribs);

	if (m_egl_ctx == nullptr)
		throw std::runtime_error("Failed to create an EGL context.");

	if (eglGetError() != EGL_SUCCESS)
		throw std::runtime_error("Received an EGL error.");
}

pm::DRMConfiguration::~DRMConfiguration()
{
	eglDestroyContext(m_egl_display, m_egl_ctx);
	eglTerminate(m_egl_display);
	gbm_device_destroy(m_gbm_device);
	int error = drmDropMaster(m_fd);
	if (error != 0) {
		std::cout << "Warning: Failed to drop the DRM master - " <<
		             std::strerror(errno) << std::endl;
	}
}

auto drm_to_phasor_mode(const drmModeModeInfo &mode_info) -> ph::DisplayMode
{
	pg::Size size = { mode_info.hdisplay, mode_info.vdisplay };
	int rate = mode_info.vrefresh;
	return ph::DisplayMode { size, rate };
}

auto pm::DRMConfiguration::scan_displays() -> ph::NativeDisplayList
{
	ph::NativeDisplayList displays;
	m_drm_resources = std::make_shared<pm::DRMModeResources>(m_fd);

	m_drm_resources->for_each_connector_id([&](uint32_t id,
	                                       const DRMModeConnectorUPtr& c) {
		if (c->connection == DRM_MODE_DISCONNECTED)
			return;
		std::vector<DisplayMode> modes;
		for (int i = 0; i < c->count_modes; i++) {
			modes.push_back(drm_to_phasor_mode(c->modes[i]));
		}
		std::string display_name = connector_name(c);
		pg::Length width = pg::Length(c->mmWidth, pg::Length::millimeters);
		pg::Length height = pg::Length(c->mmHeight, pg::Length::millimeters);
		displays.push_back(std::make_shared<NativeDisplay>(modes, 3,
		                                                   display_name,
		                                                   width, height,
		                                                   id));
	});

	return displays;
}

typedef struct {
	pm::DRMConfiguration *drm_config;
	std::weak_ptr<ph::Display> display;
} PageFlipInfo;

void page_flip_handler(int, unsigned int, unsigned int, unsigned int, void *);

int createPixel(int r, int g, int b, int a) {
  return ((a & 0xff) << 24)
       | ((r & 0xff) << 16)
       | ((g & 0xff) << 8)
       | ((b & 0xff));
}

void pm::DRMConfiguration::apply_display_configuration(
        const phasor::DisplayList &list)
{
	for (std::shared_ptr<Display> display : list) {
		std::cout << "Configuring display: " << display->name() << std::endl;
		std::shared_ptr<NativeDisplay> native =
		        std::static_pointer_cast<NativeDisplay>(display->native_display());
		std::cout << "Connector id: " << native->m_connector_id << std::endl;
		native->m_fd = m_fd;
		// Get the connector.
		DRMModeConnectorUPtr connector = m_drm_resources->connector(native->m_connector_id);
		if (connector == nullptr)
			throw std::logic_error("Native display has invalid connector ID.");
		DRMModeCrtcUPtr crtc = find_crtc_for_connector(m_fd, connector);
		native->m_crtc_id = crtc->crtc_id;
		if (native->get_active_mode() >= (int)native->modes().size())
			throw std::logic_error("Requested invalid mode.");
		if (native->get_active_mode() == -1)
			return; // Simply ignore the display.
		pg::Size size = display->space().size;
		native->m_gbm_sfc = gbm_surface_create(m_gbm_device,
		                                       size.width.as_int(),
		                                       size.height.as_int(),
		                                       GBM_FORMAT_XRGB8888,
		                                       GBM_BO_USE_SCANOUT |
		                                       GBM_BO_USE_RENDERING);
		if (native->m_gbm_sfc == nullptr)
			throw std::runtime_error("Failed to create a GBM surface.");

		native->m_egl_sfc = eglCreateWindowSurface(m_egl_display,
		                                           m_egl_config,
		                                           native->m_gbm_sfc,
		                                           nullptr);
		if (native->m_egl_sfc == EGL_NO_SURFACE)
			throw std::runtime_error("Failed to create an EGL surface.");

		eglMakeCurrent(m_egl_display, native->m_egl_sfc,
		               native->m_egl_sfc, m_egl_ctx);
		if (eglGetError() != EGL_SUCCESS)
			throw std::runtime_error("EGL error while making the context current.");

		glClearColor(0.7f, 0.0f, 1.0f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT);
		eglSwapBuffers(m_egl_display, native->m_egl_sfc);

		native->m_bo = gbm_surface_lock_front_buffer(native->m_gbm_sfc);
		uint32_t handle = gbm_bo_get_handle(native->m_bo).u32;
		uint32_t stride = gbm_bo_get_stride(native->m_bo);
		int result = drmModeAddFB(m_fd,
		                          size.width.as_int(),
		                          size.height.as_int(),
		                          24, 32,
		                          stride, handle,
		                          &native->m_fb_id);
		if (result != 0)
			throw std::runtime_error("Failed to create a DRM framebuffer.");

		result = drmModeSetCrtc(m_fd,
		                        crtc->crtc_id,
		                        native->m_fb_id,
		                        0, 0,
		                        &connector->connector_id,
		                        1,
		                        &connector->modes[native->get_active_mode()]);
		if (result != 0)
			throw std::runtime_error("Failed to change the display resolution.");
		gbm_surface_release_buffer(native->m_gbm_sfc, native->m_bo);

		PageFlipInfo *info = new PageFlipInfo;
		info->display = display;
		info->drm_config = this;
		page_flip_handler(m_fd, 0, 0, 0, info);
	}
}

void page_flip_handler(int /* fd */,
                              unsigned int /* frame */,
                              unsigned int /* sec */,
                              unsigned int /* usec */,
                              void *data)
{
	PageFlipInfo *info = static_cast<PageFlipInfo*>(data);
	std::shared_ptr<ph::Display> display = info->display.lock();
	info->drm_config->update_display(display, data);
}

void pm::DRMConfiguration::update_displays()
{
	drmEventContext event_context;
	event_context.page_flip_handler = page_flip_handler;
	event_context.version = DRM_EVENT_CONTEXT_VERSION;
	drmHandleEvent(m_fd, &event_context);
}

void pm::DRMConfiguration::update_display(std::shared_ptr<phasor::Display> display,
                                          void *data)
{
	std::shared_ptr<pm::NativeDisplay> native =
	        std::static_pointer_cast<pm::NativeDisplay>(display->native_display());
	drmModePageFlip(m_fd,
	                native->m_crtc_id,
	                native->m_fb_id,
	                DRM_MODE_PAGE_FLIP_EVENT,
	                data);
	if (native->m_bo != NULL)
		gbm_surface_release_buffer(native->m_gbm_sfc, native->m_bo);
	native->m_bo = native->m_next_bo;
	eglMakeCurrent(m_egl_display,
	               native->m_egl_sfc,
	               native->m_egl_sfc,
	               m_egl_ctx);
	glViewport(0, 0,
	           display->space().size.width.as_int(),
	           display->space().size.height.as_int());
	display->render();
	eglSwapBuffers(m_egl_display, native->m_egl_sfc);
	native->m_next_bo = gbm_surface_lock_front_buffer(native->m_gbm_sfc);
}
