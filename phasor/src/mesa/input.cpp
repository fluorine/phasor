#include <phasor/mesa/input.h>
#include <phasor/mesa/udev-wrapper.h>
#include <phasor/compositor.h>
#include <libinput.h>
#include <fcntl.h>
#include <unistd.h>
#include <iostream>
namespace ph = phasor;
namespace pm = phasor::mesa;
namespace pi = phasor::input;

int open_restricted(const char* path, int flags, void*)
{
	int result = open(path, flags);
	if (result < 0)
		throw std::runtime_error("open_restricted() failed for libinput.");
	return result;
}

void close_restricted(int fd, void*)
{
	close(fd);
}

struct libinput_interface phasor_input_interface = {
	open_restricted,
	close_restricted
};

pm::Input::Input()
{
	m_udev = std::make_shared<pm::udev::Context>();
	m_libinput = libinput_udev_create_context(&phasor_input_interface,
	                                          nullptr,
	                                          m_udev->ctx());
	if (m_libinput == nullptr)
		throw std::runtime_error("Failed to create an input context.");

	if (libinput_udev_assign_seat(m_libinput, "seat0") == -1)
		throw std::runtime_error("Failed to assign the input seat \"seat0\".");
}

pm::Input::~Input()
{
	libinput_unref(m_libinput);
}

void pm::Input::update_input(std::shared_ptr<Compositor> compositor)
{
	if (libinput_dispatch(m_libinput) < 0)
		throw std::runtime_error("libinput_dispatch() failed.");
	libinput_event *e;
	while ((e = libinput_get_event(m_libinput))) {
		process_event(compositor, e);
	}
}

void pm::Input::process_event(std::shared_ptr<Compositor> compositor,
                              libinput_event *e)
{
	switch (libinput_event_get_type(e)) {
	case LIBINPUT_EVENT_POINTER_MOTION: {
		struct pi::PointerMotion pm;
		libinput_event_pointer *pe = libinput_event_get_pointer_event(e);
		pm.time = libinput_event_pointer_get_time(pe);
		pm.time_usec = libinput_event_pointer_get_time_usec(pe);
		pm.dx = libinput_event_pointer_get_dx(pe);
		pm.dy = libinput_event_pointer_get_dy(pe);
		compositor->handle_pointer_motion(&pm);
		return;
	}
	case LIBINPUT_EVENT_NONE:
	default:
		return;
	}
}
