#include <phasor/mesa/native-display.h>
#include <phasor/mesa/gbm-buffer.h>
namespace ph = phasor;
namespace pm = phasor::mesa;
namespace pg = phasor::geometry;

pm::NativeDisplay::NativeDisplay(std::vector<DisplayMode> modes,
                                 int active_mode, std::string name,
                                 geometry::Length width,
                                 geometry::Length height,
                                 uint32_t connector_id) :
    m_modes(modes),
    m_active_mode(active_mode),
    m_name(name),
    m_width(width),
    m_height(height),
    m_connector_id(connector_id)
{
}

auto pm::NativeDisplay::modes() const -> std::vector<DisplayMode>
{
	return m_modes;
}

void pm::NativeDisplay::set_active_mode(int index)
{
	m_active_mode = index;
}

auto pm::NativeDisplay::get_active_mode() const -> int
{
	return m_active_mode;
}

auto pm::NativeDisplay::name() const -> std::string
{
	return m_name;
}

auto pm::NativeDisplay::width() const -> geometry::Length
{
	return m_width;
}

auto pm::NativeDisplay::height() const -> geometry::Length
{
	return m_height;
}

void pm::NativeDisplay::set_cursor(std::shared_ptr<ph::Buffer> buffer)
{
	// Assume the buffer is a GBM buffer.
	m_cursor_buffer =
	        std::dynamic_pointer_cast<pm::GBMBuffer>(buffer);
	show_cursor();
}

void pm::NativeDisplay::move_cursor(geometry::Point new_position)
{
	drmModeMoveCursor(m_fd, m_crtc_id,
	                  new_position.x.as_int(),
	                  new_position.y.as_int());
}

void pm::NativeDisplay::hide_cursor()
{
	drmModeSetCursor(m_fd, m_crtc_id,
	                 0, 0, 0);
}

void pm::NativeDisplay::show_cursor()
{
	if (m_cursor_buffer == nullptr)
		return;

	uint32_t handle = m_cursor_buffer->handle().u32;
	pg::Size size = m_cursor_buffer->size();
	int result = drmModeSetCursor(m_fd, m_crtc_id, handle,
	                             (uint32_t)size.width.as_int(),
	                             (uint32_t)size.height.as_int());
	if (result != 0)
		throw std::runtime_error("Failed to set the DRM hardware cursor.");
}
