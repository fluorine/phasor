#include <phasor/mesa/drm-helpers.h>
#include <phasor/mesa/udev-wrapper.h>

namespace pm = phasor::mesa;

auto pm::DRMHelpers::find_primary_gpu() const -> std::string
{
	auto context = std::make_shared<pm::udev::Context>();
	auto enumerator = std::make_shared<pm::udev::Enumerator>(context);

	enumerator->match_subsystem("drm");
	enumerator->match_sysname("card[0-9]*");
	enumerator->scan_devices();

	for (auto it = enumerator->begin(); it != enumerator->end(); it++) {
		try {
			auto parent = it->parent();
			const char *id = parent->sys_attribute("boot_vga");
			if (id && id[0] == '1') {
				return it->devnode();
			}
		} catch(...) {
			// If anything fails, this is not the device we're looking for.
		}
	}

	throw std::runtime_error("Failed to identify the primary GPU.");
}
