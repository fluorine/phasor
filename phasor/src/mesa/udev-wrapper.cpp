#include <phasor/mesa/udev-wrapper.h>
#include <libudev.h>
#include <cstring>

namespace pmu = phasor::mesa::udev;

/////////////////////
//    Device
/////////////////////

namespace
{
class DeviceImpl : public pmu::Device
{
public:
	DeviceImpl(udev_device *dev);
	virtual ~DeviceImpl() noexcept;

	virtual char const* subsystem() const override;
	virtual char const* devtype() const override;
	virtual char const* devpath() const override;
	virtual char const* devnode() const override;
	virtual char const* property(char const*) const override;
	virtual char const* sys_attribute(const char *name) const override;
	virtual std::shared_ptr<Device> parent() const override;
	udev_device* const dev;
};

DeviceImpl::DeviceImpl(udev_device *dev)
    : dev(dev)
{
	if (!dev)
		throw std::runtime_error("Udev device does not exist");

	udev_ref(udev_device_get_udev(dev));
}

DeviceImpl::~DeviceImpl() noexcept
{
	udev_unref(udev_device_get_udev(dev));
	udev_device_unref(dev);
}

char const* DeviceImpl::subsystem() const
{
	return udev_device_get_subsystem(dev);
}

char const* DeviceImpl::devtype() const
{
	return udev_device_get_devtype(dev);
}

char const* DeviceImpl::devpath() const
{
	return udev_device_get_devpath(dev);
}

char const* DeviceImpl::devnode() const
{
	return udev_device_get_devnode(dev);
}

char const* DeviceImpl::property(char const* name) const
{
	return udev_device_get_property_value(dev, name);
}

char const* DeviceImpl::sys_attribute(const char *name) const
{
	return udev_device_get_sysattr_value(dev, name);
}

std::shared_ptr<pmu::Device> DeviceImpl::parent() const
{
	return std::make_shared<DeviceImpl>(udev_device_get_parent(dev));
}
}

bool pmu::operator==(pmu::Device const& lhs, pmu::Device const& rhs)
{
	// The device path is unique
	return strcmp(lhs.devpath(), rhs.devpath()) == 0;
}

bool pmu::operator!=(pmu::Device const& lhs, pmu::Device const& rhs)
{
	return !(lhs == rhs);
}



////////////////////////
//    Enumerator
////////////////////////

pmu::Enumerator::iterator::iterator () : entry(nullptr)
{
}

pmu::Enumerator::iterator::iterator (std::shared_ptr<Context> const& ctx,
                                     udev_list_entry* entry) :
    ctx(ctx),
    entry(entry)
{
	if (entry)
		current = ctx->device_from_syspath(udev_list_entry_get_name(entry));
}

void pmu::Enumerator::iterator::increment()
{
	entry = udev_list_entry_get_next(entry);
	if (entry)
	{
		try
		{
			current = ctx->device_from_syspath(udev_list_entry_get_name(entry));
		}
		catch (std::runtime_error)
		{
			// The Device throws a runtime_error if the device does not exist
			// This can happen if it has been removed since the iterator was
			// created. If this happens, move on to the next device.
			increment();
		}
	}
	else
	{
		current.reset();
	}
}

pmu::Enumerator::iterator& pmu::Enumerator::iterator::operator++()
{
	increment();
	return *this;
}

pmu::Enumerator::iterator pmu::Enumerator::iterator::operator++(int)
{
	auto tmp = *this;
	increment();
	return tmp;
}

bool pmu::Enumerator::iterator::operator==(
        pmu::Enumerator::iterator const& rhs) const
{
	return this->entry == rhs.entry;
}

bool pmu::Enumerator::iterator::operator!=(
        pmu::Enumerator::iterator const& rhs) const
{
	return !(*this == rhs);
}

pmu::Device const& pmu::Enumerator::iterator::operator*() const
{
	return *current;
}

pmu::Device const* pmu::Enumerator::iterator::operator->() const
{
	return current.get();
}

pmu::Enumerator::Enumerator(std::shared_ptr<Context> const& ctx) :
    ctx(ctx),
    enumerator(udev_enumerate_new(ctx->ctx())),
    scanned(false)
{
}

pmu::Enumerator::~Enumerator() noexcept
{
	udev_enumerate_unref(enumerator);
}

void pmu::Enumerator::scan_devices()
{
	udev_enumerate_scan_devices(enumerator);
	scanned = true;
}

void pmu::Enumerator::match_subsystem(std::string const& subsystem)
{
	udev_enumerate_add_match_subsystem(enumerator, subsystem.c_str());
}

void pmu::Enumerator::match_parent(pmu::Device const& parent)
{
	udev_enumerate_add_match_parent(enumerator,
	                                dynamic_cast<DeviceImpl const&>(parent).dev);
}

void pmu::Enumerator::match_sysname(std::string const& sysname)
{
	udev_enumerate_add_match_sysname(enumerator, sysname.c_str());
}

pmu::Enumerator::iterator pmu::Enumerator::begin()
{
	if (!scanned)
		throw std::logic_error("Attempted to iterate over"
	                           " udev devices without first scanning");

	return iterator(ctx,
	                udev_enumerate_get_list_entry(enumerator));
}

pmu::Enumerator::iterator pmu::Enumerator::end()
{
	return iterator();
}

///////////////////
//   Context
///////////////////

pmu::Context::Context()
    : context(udev_new())
{
	if (!context)
		throw std::runtime_error("Failed to create udev context");
}

pmu::Context::~Context() noexcept
{
	udev_unref(context);
}

std::shared_ptr<pmu::Device> pmu::Context::device_from_syspath(
        std::string const& syspath)
{
	return std::make_shared<DeviceImpl>(
	            udev_device_new_from_syspath(context, syspath.c_str()));
}

udev* pmu::Context::ctx() const
{
	return context;
}

///////////////////
//   Monitor
///////////////////
pmu::Monitor::Monitor(pmu::Context const& ctx)
    : monitor(udev_monitor_new_from_netlink(ctx.ctx(), "udev")),
      enabled(false)
{
	if (!monitor)
		throw std::runtime_error("Failed to create udev_monitor");

	udev_ref(udev_monitor_get_udev(monitor));
}

pmu::Monitor::~Monitor() noexcept
{
	udev_unref(udev_monitor_get_udev(monitor));
	udev_monitor_unref(monitor);
}

void pmu::Monitor::enable(void)
{
	udev_monitor_enable_receiving(monitor);
	enabled = true;
}

static pmu::Monitor::EventType action_to_event_type(const char* action)
{
	if (strcmp(action, "add") == 0)
		return pmu::Monitor::EventType::ADDED;
	if (strcmp(action, "remove") == 0)
		return pmu::Monitor::EventType::REMOVED;
	if (strcmp(action, "change") == 0)
		return pmu::Monitor::EventType::CHANGED;
	throw std::runtime_error(std::string("Unknown udev action encountered: ") +
	                         action);
}

void pmu::Monitor::process_events(
        std::function<void(pmu::Monitor::EventType,
                           pmu::Device const&)> const& handler) const
{
	udev_device *dev;
	do
	{
		dev = udev_monitor_receive_device(monitor);
		if (dev != nullptr)
			handler(action_to_event_type(udev_device_get_action(dev)),
			        DeviceImpl(dev));
	} while (dev != nullptr);
}

int pmu::Monitor::fd(void) const
{
	return udev_monitor_get_fd(monitor);
}

void pmu::Monitor::filter_by_subsystem(std::string const& subsystem)
{
	udev_monitor_filter_add_match_subsystem_devtype(monitor, subsystem.c_str(),
	                                                nullptr);
	if (enabled)
		udev_monitor_filter_update(monitor);
}

void pmu::Monitor::filter_by_subsystem_and_type(std::string const& subsystem,
                                                std::string const& devtype)
{
	udev_monitor_filter_add_match_subsystem_devtype(monitor,
	                                                subsystem.c_str(),
	                                                devtype.c_str());
	if (enabled)
		udev_monitor_filter_update(monitor);
}
