#include <phasor/geometry/dimensions.h>
#include <phasor/geometry/point.h>
#include <phasor/geometry/size.h>
#include <phasor/geometry/displacement.h>
#include <phasor/geometry/rectangle.h>
#include <iostream>
using namespace phasor::geometry;

namespace phasor {
namespace geometry {

std::ostream& operator << (std::ostream& os, Point const& rhs)
{
	os << "(" << rhs.x << ", " << rhs.y << ")";
	return os;
}

std::ostream& operator << (std::ostream& os, Size const& rhs)
{
	os << "(" << rhs.width << " x " << rhs.height << ")";
	return os;
}

std::ostream& operator << (std::ostream& os, Displacement const& d)
{
	os << "D(" << d.dx << ", " << d.dy << ")";
	return os;
}

std::ostream& operator << (std::ostream& os, Rectangle const& rect)
{
	os << "[" << rect.top_left << ":" << rect.size << "]";
	return os;
}

}
}
