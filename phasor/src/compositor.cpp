#include <phasor/compositor.h>
#include <cmath>
#include <iostream>
namespace ph = phasor;
namespace pg = phasor::geometry;
namespace pi = phasor::input;

void ph::Compositor::handle_pointer_position(phasor::geometry::Point new_pos,
                                         std::shared_ptr<phasor::Display> display)
{
}

void ph::Compositor::handle_pointer_motion(pi::PointerMotion *e)
{
	std::shared_ptr<ph::Display> active_dpy = m_current_display.lock();
	float pointer_acceleration = 1.0f;
	pg::DeltaX dx { e->dx * 0.01f *
		            pointer_acceleration * active_dpy->dpi_horizontal() };
	pg::DeltaY dy { e->dy * 0.01f *
		            pointer_acceleration * active_dpy->dpi_vertical() };

	pg::Point new_pointer_position = m_pointer_position + dx + dy;
	// Check if the pointer has gone through any edge of the display.
	if (new_pointer_position.x > active_dpy->space().right()) {
		auto next_dpy = next_display_right(new_pointer_position);
		if (next_dpy == nullptr) {
			new_pointer_position.x = active_dpy->space().right();
		} else {
			active_dpy->native_display()->hide_cursor();
			m_current_display = active_dpy = next_dpy;
			active_dpy->native_display()->show_cursor();
			/// @todo Issue an event to the compositor that the active display
			/// has changed.
		}
	}

	if (new_pointer_position.y > active_dpy->space().bottom()) {
		new_pointer_position.y = active_dpy->space().bottom();
		auto next_dpy = next_display_below(new_pointer_position);
		if (next_dpy == nullptr) {
			new_pointer_position.y = active_dpy->space().bottom();
		} else {
			active_dpy->native_display()->hide_cursor();
			m_current_display = active_dpy = next_dpy;
			active_dpy->native_display()->show_cursor();
		}
	}

	if (new_pointer_position.x.as_int() < 0) {
		auto next_dpy = next_display_left(new_pointer_position);
		if (next_dpy == nullptr) {
			new_pointer_position.x = 0;
		} else {
			active_dpy->native_display()->hide_cursor();
			m_current_display = active_dpy = next_dpy;
			active_dpy->native_display()->show_cursor();
		}
	}

	if (new_pointer_position.y.as_int() < 0) {
		auto next_dpy = next_display_above(new_pointer_position);
		if (next_dpy == nullptr) {
			new_pointer_position.y = 0;
		} else {
			active_dpy->native_display()->hide_cursor();
			m_current_display = active_dpy = next_dpy;
			active_dpy->native_display()->show_cursor();
		}
	}

	active_dpy->native_display()->move_cursor(new_pointer_position);
	m_pointer_position = new_pointer_position;
}

inline auto almost_equal(float a, float b) -> bool {
	return fabs(a - b) < 0.125f; // Unlikely that a display will be less than
	                             // 1/8'' long.
}

inline auto clamp_zero(float a) -> float {
	if (a < 0.0f)
		return 0.0f;
	return a;
}

auto ph::Compositor::next_display_right(pg::Point &pointer_pos)
    -> std::shared_ptr<ph::Display>
{
	std::shared_ptr<ph::Display> active_dpy = m_current_display.lock();
	pg::Length global_x_pos = active_dpy->left_offset() + active_dpy->width();
	pg::Length global_y_pos = active_dpy->top_offset() +
	        pg::Length(pointer_pos.y.as_float() / active_dpy->dpi_vertical(),
	                   pg::Length::inches);
	for (std::shared_ptr<ph::Display> dpy : m_display_list) {
		if (dpy == active_dpy)
			continue;
		if (almost_equal(global_x_pos.as(pg::Length::inches),
		                 dpy->left_offset().as(pg::Length::inches))) {
			pg::Y new_y_pos = (global_y_pos - dpy->top_offset())
			        .as_pixels(dpy->dpi_vertical());
			if ((new_y_pos >= pg::Y { 0 }) &&
			        (new_y_pos < dpy->space().bottom())) {
				pointer_pos.x = pg::X { 0 };
				pointer_pos.y = new_y_pos;
				return dpy;
			}
		}
	}
	return nullptr;
}

auto ph::Compositor::next_display_left(pg::Point &pointer_pos)
    -> std::shared_ptr<ph::Display>
{
	std::shared_ptr<ph::Display> active_dpy = m_current_display.lock();
	pg::Length global_x_pos = active_dpy->left_offset();
	pg::Length global_y_pos = active_dpy->top_offset() +
	        pg::Length(pointer_pos.y.as_float() / active_dpy->dpi_vertical(),
	                   pg::Length::inches);
	for (std::shared_ptr<ph::Display> dpy : m_display_list) {
		if (dpy == active_dpy)
			continue;
		if (almost_equal(global_x_pos.as(pg::Length::inches),
		                 (dpy->left_offset() + dpy->width())
		                    .as(pg::Length::inches))) {
			pg::Y new_y_pos = (global_y_pos - dpy->top_offset())
			        .as_pixels(dpy->dpi_vertical());
			if ((new_y_pos >= pg::Y { 0 }) &&
			        (new_y_pos < dpy->space().bottom())) {
				pointer_pos.x = dpy->space().right();
				pointer_pos.y = new_y_pos;
				return dpy;
			}
		}
	}
	return nullptr;
}

auto ph::Compositor::next_display_above(geometry::Point &pos)
    -> std::shared_ptr<ph::Display>
{
	std::shared_ptr<ph::Display> active_dpy = m_current_display.lock();
	pg::Length global_x_pos = active_dpy->left_offset() +
	        pg::Length(pos.x.as_float() / active_dpy->dpi_horizontal(),
	                   pg::Length::inches);
	pg::Length global_y_pos = active_dpy->top_offset();
	for (std::shared_ptr<ph::Display> dpy : m_display_list) {
		if (dpy == active_dpy)
			continue;
		if (almost_equal(global_y_pos.as(pg::Length::inches),
		                 (dpy->top_offset() + dpy->height())
		                    .as(pg::Length::inches)))  {
			pg::X new_x_pos = (global_x_pos - dpy->left_offset())
			        .as_pixels(dpy->dpi_horizontal());
			if ((new_x_pos >= pg::X {0}) &&
			        (new_x_pos <= dpy->space().right())) {
				pos.x = new_x_pos;
				pos.y = dpy->space().bottom();
				return dpy;
			}
		}
	}
	return nullptr;
}

auto ph::Compositor::next_display_below(geometry::Point &pos)
    -> std::shared_ptr<ph::Display>
{
	std::shared_ptr<ph::Display> active_dpy = m_current_display.lock();
	pg::Length global_x_pos = active_dpy->left_offset() +
	        pg::Length(pos.x.as_float() / active_dpy->dpi_horizontal(),
	                   pg::Length::inches);
	pg::Length global_y_pos = active_dpy->top_offset() + active_dpy->height();
	for (std::shared_ptr<ph::Display> dpy : m_display_list) {
		if (dpy == active_dpy)
			continue;
		if (almost_equal(global_y_pos.as(pg::Length::inches),
		                 dpy->top_offset().as(pg::Length::inches)))  {
			pg::X new_x_pos = (global_x_pos - dpy->left_offset())
			        .as_pixels(dpy->dpi_horizontal());
			if ((new_x_pos >= pg::X {0}) &&
			        (new_x_pos <= dpy->space().right())) {
				pos.x = new_x_pos;
				pos.y = dpy->space().top();
				return dpy;
			}
		}
	}
	return nullptr;
}
