#include <phasor/log.h>
#include <iostream>

void phasor::log(Severity severity, std::string message, std::string component)
{
	switch (severity) {
	case phasor::DEBUG:
		std::cout << "Debug";
		break;
	case phasor::ERROR:
		std::cout << "Error";
		break;
	case phasor::INFO:
		std::cout << "Information";
		break;
	case phasor::WARNING:
		std::cout << "Warning";
	}

	std::cout << ": [" << component << "] - " << message << std::endl;
}
