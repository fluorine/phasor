#include <phasor/gl/simplerenderer.h>
#include <phasor/gl/renderable.h>
#include <phasor/gl/texture.h>
#include <glbinding/gl/gl.h>
#include <glm/gtc/matrix_transform.hpp>

namespace pg = phasor::geometry;
namespace pgl = phasor::gl;

static const std::string vertex_source =
        "#version 300 es\n"
        "layout(location = 0) in vec2 in_pos;\n"
        "layout(location = 1) in vec2 in_uv;\n"
        "uniform mat4 view_mat;\n"
        "out vec2 uv;\n"
        "void main() {\n"
        "\tgl_Position = view_mat * vec4(in_pos, 0.0, 1.0);\n"
        "uv = in_uv;\n"
        "}\n";

static const std::string fragment_source =
        "#version 300 es\n"
        "precision mediump float;\n"
        "uniform sampler2D surface_tex;\n"
        "in vec2 uv;\n"
        "out mediump vec4 color;\n"
        "void main() {\n"
        "\tcolor = texture2D(surface_tex, uv);\n"
        "}\n";

pgl::SimpleRenderer::SimpleRenderer(pg::Size display_size)
{
	m_program = std::make_shared<pgl::Program>(vertex_source, fragment_source);
	m_program->use();
	m_contents_uniform = m_program->find_uniform("surface_tex");
	m_matrix_uniform = m_program->find_uniform("view_mat");
	m_view_matrix = glm::ortho(
	            0.0f, display_size.width.as_float(),
	            display_size.height.as_float(), 0.0f);
}

void pgl::SimpleRenderer::start_frame()
{
	m_program->use();
	m_matrix_uniform.set(m_view_matrix);
}

void pgl::SimpleRenderer::render(Renderable *r)
{
	m_program->use();
	m_contents_uniform.set(r->texture(), 0);
	pg::Rectangle region = r->region();
	region.size = r->texture()->size();

	/**
	 * @note Some claim that in order to have pixel-perfect rendering, one
	 * should add 0.5 to the coordinates so that the vertices fall at the center
	 * of the pixels. At least on the nested platform, the opposite happens,
	 * with blurry windows if 0.5 is added. Testing will show wether that's
	 * necessary for the native platform.
	 */

	const float left = region.left().as_float();
	const float right = region.right().as_float();
	const float top = region.top().as_float();
	const float bottom = region.bottom().as_float();
	const float pos_buffer[] = {
	    left, bottom, 	// Bottom-left
	    right, bottom,	// Bottom-right
	    right, top,		// Top-right
	    left, bottom,	// Bottom-left
	    right, top,		// Top-right
	    left, top		// Top-left
	};

	static const float uv_buffer[] = {
	    0.0f, 1.0f,
	    1.0f, 1.0f,
	    1.0f, 0.0f,
	    0.0f, 1.0f,
	    1.0f, 0.0f,
	    0.0f, 0.0f
	};

	::gl::glEnableVertexAttribArray(0);
	::gl::glEnableVertexAttribArray(1);
	::gl::glVertexAttribPointer(0, 2, ::gl::GL_FLOAT, ::gl::GL_FALSE,
	                            0, pos_buffer);
	::gl::glVertexAttribPointer(1, 2, ::gl::GL_FLOAT, ::gl::GL_TRUE,
	                            0, uv_buffer);

	::gl::glEnable(::gl::GL_BLEND);
	::gl::glBlendFunc(::gl::GL_SRC_ALPHA, ::gl::GL_ONE_MINUS_SRC_ALPHA);
	::gl::glDrawArrays(::gl::GL_TRIANGLES, 0, 6);
}
