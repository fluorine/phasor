#include <phasor/gl/program.h>
#include <phasor/gl/shader.h>
#include <phasor/gl/texture.h>
#include <phasor/log.h>
#include <glbinding/gl/gl.h>
#include <assert.h>
#include <stdexcept>
#include <fstream>
#include <sstream>
#include <glm/gtc/type_ptr.hpp>

using namespace gl;
using namespace phasor::gl;

Program::Program(std::string vertex_src, std::string fragment_src)
{
	auto vertex_shader = std::make_shared<Shader>(Shader::VERTEX, vertex_src);
	auto fragment_shader = std::make_shared<Shader>(Shader::FRAGMENT,
	                                               fragment_src);

	m_id = glCreateProgram();
	assert(m_id > 0);

	glAttachShader(m_id, vertex_shader->id());
	glAttachShader(m_id, fragment_shader->id());
	glLinkProgram(m_id);

	GLint success;
	glGetProgramiv(m_id, GL_LINK_STATUS, &success);
	if (!success) {
		GLchar infoLog[512];
		glGetProgramInfoLog(m_id, 512, NULL, infoLog);
		std::string text = "Program linking failed - Info Log:\n";
		text.append(infoLog);
		phasor::log(phasor::ERROR, text, "gl");
		throw std::runtime_error("Failed to link shader program.");
	}
}

Program::~Program()
{
	glDeleteProgram(m_id);
}

void Program::use()
{
	glUseProgram(m_id);
}

void Program::release()
{
	glUseProgram(0);
}

Program::Uniform::Uniform(GLint id) :
    m_id(id)
{
	assert(m_id > -1);
}

Program::Uniform::Uniform() : m_id(-1)
{
	// Default constructor, not to be used.
}

Program::Uniform Program::find_uniform(std::string name) const
{
	return Uniform(glGetUniformLocation(m_id, name.c_str()));
}

/// @note For OpenGL 4 we should transition to glProgramUniform{...}()
void Program::Uniform::set(float v) const { glUniform1f(m_id, v); }
void Program::Uniform::set(double v) const { glUniform1d(m_id, v); }
void Program::Uniform::set(int v) const { glUniform1i(m_id, v); }

void Program::Uniform::set(std::shared_ptr<Texture> texture, int unit) const
{
	glActiveTexture(GL_TEXTURE0 + unit);
	texture->bind();
	glUniform1i(m_id, unit);
}

void Program::Uniform::set(glm::vec2 v) const
{
	glUniform2f(m_id, v.x, v.y);
}

void Program::Uniform::set(glm::dvec2 v) const
{
	glUniform2d(m_id, v.x, v.y);
}

void Program::Uniform::set(glm::ivec2 v) const
{
	glUniform2i(m_id, v.x, v.y);
}

void Program::Uniform::set(glm::mat2 m) const
{
	glUniformMatrix2fv(m_id, 1, GL_FALSE, glm::value_ptr(m));
}

void Program::Uniform::set(glm::mat4 m) const
{
	glUniformMatrix4fv(m_id, 1, GL_FALSE, glm::value_ptr(m));
}
