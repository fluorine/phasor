#include <phasor/gl/texture.h>
#include <stdexcept>
#include <glbinding/gl/gl.h>

namespace pg = phasor::geometry;
namespace pgl = phasor::gl;

pgl::Texture::Texture() :
    m_id(generate_id())
{
	if (m_id == 0)
		throw std::runtime_error("Failed to generate a valid texture ID.");
	::gl::glBindTexture(::gl::GL_TEXTURE_2D, m_id);

	::gl::glTexParameteri(::gl::GL_TEXTURE_2D,
	                      ::gl::GL_TEXTURE_WRAP_S, ::gl::GL_CLAMP_TO_EDGE);

	::gl::glTexParameteri(::gl::GL_TEXTURE_2D,
	                      ::gl::GL_TEXTURE_WRAP_T, ::gl::GL_CLAMP_TO_EDGE);

	::gl::glTexParameteri(::gl::GL_TEXTURE_2D,
	                      ::gl::GL_TEXTURE_MIN_FILTER, ::gl::GL_LINEAR);

	::gl::glTexParameteri(::gl::GL_TEXTURE_2D,
	                      ::gl::GL_TEXTURE_MAG_FILTER, ::gl::GL_LINEAR);
}

pgl::Texture::~Texture()
{
	::gl::glDeleteTextures(1, &m_id);
}

void pgl::Texture::bind()
{
	::gl::glBindTexture(::gl::GL_TEXTURE_2D, m_id);
}

void pgl::Texture::set_data(geometry::Size size, void *data)
{
	bind();
	m_size = size;
	::gl::glTexImage2D(::gl::GL_TEXTURE_2D, 0, ::gl::GL_RGBA,
	                   size.width.as_int(), size.height.as_int(),
	                   0, ::gl::GL_BGRA, ::gl::GL_UNSIGNED_BYTE, data);
}

auto pgl::Texture::generate_id() -> ::gl::GLuint
{
	::gl::GLuint id = 0;
	::gl::glGenTextures(1, &id);
	return id;
}
