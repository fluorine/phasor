#include <phasor/fd.h>
#include <unistd.h>

namespace ph = phasor;

ph::Fd::Fd() :
    m_fd(invalid)
{
}

ph::Fd::Fd(int fd) :
    m_fd(fd)
{
}

ph::Fd::~Fd()
{
	close();
}

void ph::Fd::operator =(int fd)
{
	close();
	m_fd = fd;
}

void ph::Fd::close()
{
	if (m_fd != invalid) {
		::close(m_fd);
		m_fd = invalid;
	}
}
