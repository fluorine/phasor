#include <phasor/nested/platform.h>
#include <phasor/nested/nested-surface.h>
#include <phasor/compositor.h>
#include <QGuiApplication>

namespace ph = phasor;
namespace pn = phasor::nested;
namespace pi = phasor::input;
using std::shared_ptr;

pn::Platform::Platform(int *argc, char ***argv,
                       std::vector<DisplayInfo> displays)
{
	m_qt_app = new QGuiApplication(*argc, *argv);
	for (DisplayInfo info : displays) {
		m_native_displays.push_back(std::make_shared<pn::NativeDisplay>(
		                            info.size, info.name, info.dpi));
	}
}

pn::Platform::~Platform()
{
	delete m_qt_app;
}

auto pn::Platform::scan_displays() -> ph::NativeDisplayList
{
	// We can't just return m_displays since the elements are of different type.
	ph::NativeDisplayList displays;
	for (std::shared_ptr<pn::NativeDisplay> dpy : m_native_displays) {
		displays.push_back(dpy);
	}
	return displays;
}

void pn::Platform::apply_display_configuration(const DisplayList &dpy_list)
{
	for (shared_ptr<ph::Display> dpy : dpy_list) {
		shared_ptr<ph::NativeDisplay> native = dpy->native_display();
		shared_ptr<pn::NativeDisplay> nested_native =
		        std::dynamic_pointer_cast<pn::NativeDisplay>(native);
		if (nested_native->get_active_mode() != nested_native->disabled) {
			nested_native->m_display = dpy;
			nested_native->show_nested();
		}
		else
			nested_native->destroy_nested();
	}
	m_displays = dpy_list;
}

void pn::Platform::update_displays()
{
	for (std::shared_ptr<pn::NativeDisplay> dpy : m_native_displays)
		dpy->repaint();
}

void pn::Platform::update_input(std::shared_ptr<Compositor> compositor)
{
	Q_UNUSED(compositor);
	m_qt_app->processEvents();
	static QPoint last_pos = QCursor::pos();
	QPoint new_pos = QCursor::pos();
	if (last_pos != new_pos) {
		pi::PointerMotion pm;
		pm.dx = new_pos.x() - last_pos.x();
		pm.dy = new_pos.y() - last_pos.y();
		compositor->handle_pointer_motion(&pm);
		last_pos = new_pos;
	}
}
