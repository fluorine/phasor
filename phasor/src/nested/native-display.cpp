#include <phasor/nested/native-display.h>
#include <phasor/nested/nested-surface.h>
#include <iostream>
namespace ph = phasor;
namespace pg = phasor::geometry;
namespace pn = phasor::nested;

pn::NativeDisplay::NativeDisplay(geometry::Size size, std::string name,
                                 float dpi) :
    m_size(size),
    m_name(name),
    m_autoAdjustDPI(dpi == 0.0)
{
	if (dpi > 0.0) {
		m_width = pg::Length(size.width.as_float() / dpi, pg::Length::inches);
		m_height = pg::Length(size.height.as_float() / dpi, pg::Length::inches);
	}
}

auto pn::NativeDisplay::modes() const -> std::vector<ph::DisplayMode>
{
	ph::DisplayMode mode = { m_size, 60 };
	std::vector<ph::DisplayMode> mode_list;
	mode_list.push_back(mode);
	return mode_list;
}

void pn::NativeDisplay::set_active_mode(int index)
{
	m_active = index > -1;
}

auto pn::NativeDisplay::get_active_mode() const -> int
{
	return m_active ? 0 : -1;
}

auto pn::NativeDisplay::name() const -> std::string
{
	return m_name;
}

auto pn::NativeDisplay::width() const -> pg::Length
{
	return m_width;
}

auto pn::NativeDisplay::height() const -> pg::Length
{
	return m_height;
}

void pn::NativeDisplay::set_cursor(std::shared_ptr<ph::Buffer> /* buffer */)
{
}

void pn::NativeDisplay::move_cursor(pg::Point /* new_position */)
{
}

void pn::NativeDisplay::hide_cursor()
{
}

void pn::NativeDisplay::show_cursor()
{
}

void pn::NativeDisplay::show_nested()
{
	m_surface = std::make_shared<pn::NestedSurface>();
	m_surface->m_display = m_display;
	m_surface->setTitle(QString("Phasor Nested Display: ") +
	                    QString(m_name.c_str()));

	// Set the correct size.
	QSize size = QSize(m_size.width.as_int(), m_size.height.as_int());
	m_surface->setMinimumSize(size);
	m_surface->setMaximumSize(size);
	m_surface->show();

	// Get the DPI and adjust the display size accordingly.
	if (m_autoAdjustDPI) {
		float hdpi = m_surface->devicePixelRatioF();
		float vdpi = m_surface->devicePixelRatioF();
		m_width = pg::Length(m_size.width.as_float() / hdpi, pg::Length::inches);
		m_height = pg::Length(m_size.height.as_float() / vdpi, pg::Length::inches);
	}
}

void pn::NativeDisplay::destroy_nested()
{
	if (m_surface == nullptr)
		return;
}

void pn::NativeDisplay::repaint()
{
	m_surface->update();
}
