#include <phasor/nested/nested-surface.h>
#include <phasor/display.h>
#include <QMouseEvent>

namespace ph = phasor;
namespace pn = phasor::nested;

pn::NestedSurface::NestedSurface() :
    QOpenGLWindow(QOpenGLWindow::NoPartialUpdate, nullptr)
{
	setTitle("Phasor Nested Display");

}

pn::NestedSurface::~NestedSurface()
{
}

void pn::NestedSurface::paintGL()
{
	std::shared_ptr<phasor::Display> display = m_display.lock();
	glViewport(0, 0, width(), height());
	display->render();
}

void pn::NestedSurface::mouseMoveEvent(QMouseEvent *e)
{
	if (last_mouse_X == -1) {
		// This is the first time things are set.
		last_mouse_X = e->x();
		last_mouse_Y = e->y();
		return;
	}

	int mouse_dx = e->x() - last_mouse_X;
	int mouse_dy = e->y() - last_mouse_Y;
	last_mouse_X += mouse_dx;
	last_mouse_Y += mouse_dy;
}

void pn::NestedSurface::get_mouse_delta(int &dx, int &dy)
{
	dx = mouse_dx;
	dy = mouse_dy;
	mouse_dx = 0;
	mouse_dy = 0;
}
