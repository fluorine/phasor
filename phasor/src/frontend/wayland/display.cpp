#include <phasor/frontend/wayland/display.h>
#include <phasor/frontend/wayland/surface.h>
#include <phasor/frontend/frontend.h>
#include <phasor/geometry/region.h>
#include <phasor/server.h>
#include "xdg-shell.h"
#include <assert.h>
#include <iostream>

namespace ph = phasor;
namespace pg = phasor::geometry;
namespace pf = phasor::frontend;
namespace pfw = phasor::frontend::wayland;

#define UNIMPLEMENTED(s) \
	printf("Unimplemented Wayland method reached : %s\n", s);

#define BIND_INTERFACE(s) \
	printf("Binding Wayland interface: %s\n", s);

// Wayland interfaces.
// They are defined here since they are only a thin wrapper that forwards the
// requests to the phasor::frontend::Frontend class.

static void surface_destroy(struct wl_client *client, struct wl_resource *resource)
{
	UNIMPLEMENTED("surface_destroy")
}

auto get_surface(struct wl_resource *resource) -> pfw::Surface*
{
	pfw::Surface *s = static_cast<pfw::Surface*>(
	            wl_resource_get_user_data(resource));
	assert(s != nullptr);
	return s;
}

auto get_region(struct wl_resource *resource) -> pg::Region*
{
	pg::Region *region = static_cast<pg::Region*>(
	            wl_resource_get_user_data(resource));
	assert(region != nullptr);
	return region;
}

static void surface_attach(struct wl_client *,
                           struct wl_resource *resource,
                           struct wl_resource *buffer,
                           int32_t /* x */, int32_t /* y */) //!< @todo
{
	pfw::Surface *s = get_surface(resource);
	ph::g_server->frontend().lock()->wl_surface_attach(s, buffer);
}

static void surface_damage(struct wl_client *, struct wl_resource *resource,
                           int32_t /* x */, int32_t /* y */,
                           int32_t /* width */, int32_t /* height */)
{
	/// @note We don't use the damaged region information and just reload the
	/// whole surface image. For this method only we also bypass the frontend
	/// class.
	pfw::Surface *s = get_surface(resource);
	s->set_damaged();
}

static void surface_frame(struct wl_client *client,
                          struct wl_resource *resource, uint32_t callback)
{
	pfw::Surface *s = get_surface(resource);
	wl_resource *cb = wl_resource_create(client,
	                                     &wl_callback_interface, 1, callback);
	ph::g_server->frontend().lock()->wl_surface_frame(s, cb);
}

static void surface_set_opaque_region(struct wl_client *,
                                      struct wl_resource *resource,
                                      struct wl_resource *region)
{
	pfw::Surface *s = get_surface(resource);
	pg::Region *r = (region != nullptr) ? get_region(region) : nullptr;
	ph::g_server->frontend().lock()->wl_surface_set_opaque_region(s, r);
}

static void surface_set_input_region(struct wl_client *,
                                     struct wl_resource *resource,
                                     struct wl_resource *region)
{
	pfw::Surface *s = get_surface(resource);
	pg::Region *r = (region != nullptr) ? get_region(region) : nullptr;
	ph::g_server->frontend().lock()->wl_surface_set_input_region(s, r);
}

static void surface_commit(struct wl_client *, struct wl_resource *resource)
{
	pfw::Surface *s = get_surface(resource);
	ph::g_server->frontend().lock()->wl_surface_commit(s);
}

static void surface_set_buffer_transform(struct wl_client *client, struct wl_resource *resource, int32_t transform)
{
	UNIMPLEMENTED("surface_set_buffer_transform")
}

static void surface_set_buffer_scale(struct wl_client*,
                                     struct wl_resource *resource,
                                     int32_t scale)
{
	pfw::Surface *s = get_surface(resource);
	ph::g_server->frontend().lock()->wl_surface_set_buffer_scale(s, scale);
}

static struct wl_surface_interface surface_interface = {
	&surface_destroy,
	&surface_attach,
	&surface_damage,
	&surface_frame,
	&surface_set_opaque_region,
	&surface_set_input_region,
	&surface_commit,
	&surface_set_buffer_transform,
	&surface_set_buffer_scale,
	nullptr, //!< @todo Implement wl_surface_damage_buffer()
};

static void compositor_create_surface(wl_client *c,
                                      wl_resource *,
                                      uint32_t id)
{
	wl_resource *_surface = wl_resource_create(c, &wl_surface_interface, 3, id);
	wl_resource_set_implementation(_surface,
	                               &surface_interface, nullptr, nullptr);
	phasor::g_server->frontend().lock()->wl_create_surface(c, _surface);
}


static void region_destroy(struct wl_client *, struct wl_resource *resource)
{
	pg::Region *region = get_region(resource);
	delete region;
}

static void region_add(struct wl_client *,
                       struct wl_resource *resource,
                       int32_t x, int32_t y, int32_t width, int32_t height)
{
	pg::Rectangle rect(pg::Point {x, y}, pg::Size {width, height});
	pg::Region *region = get_region(resource);
	(*region) += rect;
}

static void region_subtract(struct wl_client *,
                            struct wl_resource *resource,
                            int32_t x, int32_t y, int32_t width, int32_t height)
{
	pg::Rectangle rect(pg::Point {x, y}, pg::Size {width, height});
	pg::Region *region = get_region(resource);
	(*region) -= rect;
}

static struct wl_region_interface region_interface = {
	&region_destroy,
	&region_add,
	&region_subtract
};

static void compositor_create_region(wl_client *c,
                                     wl_resource *,
                                     uint32_t id)
{
	wl_resource *region = wl_resource_create(c, &wl_region_interface, 1, id);
	wl_resource_set_implementation(region, &region_interface, NULL, NULL);
	wl_resource_set_user_data(region, static_cast<void*>(new pg::Region()));
}

static struct wl_compositor_interface compositor_interface = {
    &compositor_create_surface,
    &compositor_create_region
};

static void compositor_bind(wl_client *client, void *data,
                            uint32_t version, uint32_t id)
{
	BIND_INTERFACE("compositor")
    wl_resource *resource = wl_resource_create(client,
	                        &wl_compositor_interface, 3, id);
	wl_resource_set_implementation(resource, &compositor_interface, NULL, NULL);
}

static void xdg_surface_destroy(struct wl_client *client, struct wl_resource *resource)
{
	UNIMPLEMENTED("xdg_surface_destroy")
}

static void xdg_surface_set_parent(struct wl_client *client, struct wl_resource *resource, struct wl_resource *parent)
{
	UNIMPLEMENTED("xdg_surface_set_parent")
}

static void xdg_surface_set_title(struct wl_client *client, struct wl_resource *resource, const char *title)
{
	UNIMPLEMENTED("xdg_surface_set_title")
	printf("Requested title: \"%s\"\n", title);
	ph::g_server->frontend().lock()->xdg_surface_set_title(client, resource, title);
}

static void xdg_surface_set_app_id(struct wl_client *client, struct wl_resource *resource, const char *app_id)
{
	UNIMPLEMENTED("xdg_surface_set_app_id")
}

static void xdg_surface_show_window_menu(struct wl_client *client, struct wl_resource *resource, struct wl_resource *seat, uint32_t serial, int32_t x, int32_t y)
{
	UNIMPLEMENTED("xdg_surface_show_window_menu")
}

static void xdg_surface_move(struct wl_client *client, struct wl_resource *resource, struct wl_resource *seat, uint32_t serial)
{
	UNIMPLEMENTED("xdg_surface_move")
}

static void xdg_surface_resize(struct wl_client *client, struct wl_resource *resource, struct wl_resource *seat, uint32_t serial, uint32_t edges)
{
	UNIMPLEMENTED("xdg_surface_resize")
}

static void xdg_surface_ack_configure(struct wl_client *client, struct wl_resource *resource, uint32_t serial)
{
	UNIMPLEMENTED("xdg_surface_ack_configure")
}

static void xdg_surface_set_window_geometry(struct wl_client *client, struct wl_resource *resource, int32_t x, int32_t y, int32_t width, int32_t height)
{
	UNIMPLEMENTED("xdg_surface_set_window_geometry")
}

static void xdg_surface_set_maximized(struct wl_client *client, struct wl_resource *resource)
{
	UNIMPLEMENTED("xdg_surface_set_maximized")
}

static void xdg_surface_unset_maximized(struct wl_client *client, struct wl_resource *resource)
{
	UNIMPLEMENTED("xdg_surface_unset_maximized")
}

static void xdg_surface_set_fullscreen(struct wl_client *client, struct wl_resource *resource, struct wl_resource *output)
{
	UNIMPLEMENTED("xdg_surface_set_fullscreen")
}

static void xdg_surface_unset_fullscreen(struct wl_client *client, struct wl_resource *resource)
{
	UNIMPLEMENTED("xdg_surface_unset_fullscreen")
}

static void xdg_surface_set_minimized(struct wl_client *client, struct wl_resource *resource)
{
	UNIMPLEMENTED("xdg_surface_set_minimized")
}

static struct xdg_surface_interface phasor_xdg_surface_interface = {
	&xdg_surface_destroy,
	&xdg_surface_set_parent,
	&xdg_surface_set_title,
	&xdg_surface_set_app_id,
	&xdg_surface_show_window_menu,
	&xdg_surface_move,
	&xdg_surface_resize,
	&xdg_surface_ack_configure,
	&xdg_surface_set_window_geometry,
	&xdg_surface_set_maximized,
	&xdg_surface_unset_maximized,
	&xdg_surface_set_fullscreen,
	&xdg_surface_unset_fullscreen,
	&xdg_surface_set_minimized
};

// xdg shell
static void xdg_shell_destroy(struct wl_client *client, struct wl_resource *resource)
{
	UNIMPLEMENTED("xdg_shell_destroy")
}

static void xdg_shell_use_unstable_version(struct wl_client *client, struct wl_resource *resource, int32_t version)
{
	if (version != 5) {
		printf("Warning: Requested unstable XDG shell version %d\n", version);
	}
}

static void xdg_shell_get_xdg_surface(struct wl_client *client, struct wl_resource *resource, uint32_t id, struct wl_resource *_surface)
{
	wl_resource *_xdg_surface =
	        wl_resource_create(client, &xdg_surface_interface, 1, id);
	wl_resource_set_implementation(_xdg_surface, &phasor_xdg_surface_interface,
	                               nullptr, nullptr);
	ph::g_server->frontend().lock()->xdg_shell_get_surface(client,
	                                                       _xdg_surface,
	                                                       _surface);
}

static void xdg_shell_get_xdg_popup(struct wl_client *client, struct wl_resource *resource, uint32_t id, struct wl_resource *surface, struct wl_resource *parent, struct wl_resource *seat, uint32_t serial, int32_t x, int32_t y)
{
	UNIMPLEMENTED("xdg_shell_get_xdg_popup")
}

static void xdg_shell_pong(struct wl_client *client, struct wl_resource *resource, uint32_t serial)
{
	UNIMPLEMENTED("xdg_shell_pong")
}

static struct xdg_shell_interface phasor_xdg_shell_interface = {
	&xdg_shell_destroy,
	&xdg_shell_use_unstable_version,
	&xdg_shell_get_xdg_surface,
	&xdg_shell_get_xdg_popup,
	&xdg_shell_pong
};

static void xdg_shell_bind (struct wl_client *client, void *data, uint32_t version, uint32_t id)
{
	BIND_INTERFACE("xdg_shell")
	struct wl_resource *resource = wl_resource_create(client, &xdg_shell_interface, 1, id);
	wl_resource_set_implementation(resource, &phasor_xdg_shell_interface, NULL, NULL);
}

pfw::Display::Display()
{
    m_display = wl_display_create();
    wl_display_add_socket_auto(m_display);
	wl_global_create(m_display, &wl_compositor_interface, 3,
                     NULL, &compositor_bind);
	wl_global_create(m_display, &xdg_shell_interface, 1, NULL, &xdg_shell_bind);
    wl_display_init_shm(m_display);
    m_event_loop = wl_display_get_event_loop(m_display);
}

pfw::Display::~Display()
{
    wl_display_destroy(m_display);
}

void pfw::Display::dispatch_events()
{
    wl_event_loop_dispatch(m_event_loop, 0);
    wl_display_flush_clients(m_display);
}

