#include <phasor/frontend/wayland/surface.h>
#include <phasor/gl/texture.h>
#include <phasor/geometry/size.h>
#include <phasor/log.h>
#include <assert.h>
#include <time.h>
#include "xdg-shell.h"

namespace ph = phasor;
namespace pg = phasor::geometry;
namespace pf = phasor::frontend;
namespace pfw = phasor::frontend::wayland;
namespace pgl = phasor::gl;

static auto get_timestamp() -> uint32_t
{
	struct timespec t;
	clock_gettime(CLOCK_MONOTONIC, &t);
	return t.tv_sec * 1000 + t.tv_nsec / 1000000;
}

pfw::Surface::~Surface() { }

void pfw::Surface::commit()
{
	if (m_texture == nullptr) {
		m_texture = std::make_shared<pgl::Texture>();
	}

	// This would mean that someone tried to commit before attaching a buffer.
	if (m_wl_buffer == nullptr) {
		ph::log(ph::WARNING,
		        "wl_surface.commit called before wl_surface.attach.",
		        "Frontend");
		return;
	}

	wl_shm_buffer *shm_buffer = wl_shm_buffer_get(m_wl_buffer);
	uint32_t width = wl_shm_buffer_get_width(shm_buffer);
	uint32_t height = wl_shm_buffer_get_height(shm_buffer);
	pg::Size new_size = pg::Size { width, height };
	void *data = wl_shm_buffer_get_data(shm_buffer);
	m_texture->set_data(new_size, data);
	wl_buffer_send_release(m_wl_buffer);
	set_damaged(false);
}

void pfw::Surface::frame_done()
{
	if (m_wl_frame_callback == nullptr) {
		// No frame callback set by the client.
		return;
	}
	wl_callback_send_done(m_wl_frame_callback, get_timestamp());
	wl_resource_destroy(m_wl_frame_callback);
	m_wl_frame_callback = nullptr;
}

void pfw::Surface::send_configure(phasor::geometry::Size size)
{
	wl_array state_array;
	wl_array_init(&state_array);
	xdg_surface_send_configure(xdg_surface(),
	                           size.width.as_int(),
	                           size.height.as_int(),
	                           &state_array, 0);
}
