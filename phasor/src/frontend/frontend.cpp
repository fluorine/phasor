#include <phasor/frontend/frontend.h>
#include <phasor/frontend/wayland/display.h>
#include <phasor/frontend/wayland/surface.h>
#include <phasor/frontend/wayland/client.h>
#include <phasor/geometry/region.h>
#include <phasor/server.h>
#include <phasor/compositor.h>
#include <wayland-server.h>
#include <algorithm>
#include <stdio.h>

namespace ph = phasor;
namespace pg = phasor::geometry;
namespace pf = phasor::frontend;
namespace pfw = phasor::frontend::wayland;

pf::Frontend::Frontend()
{
}

pf::Frontend::~Frontend()
{
}

void pf::Frontend::start()
{
	m_display = std::make_shared<pfw::Display>();
}

void pf::Frontend::stop()
{
}

void pf::Frontend::update()
{
	for (auto s : m_clients) {
		s->ping();
	}
	m_display->dispatch_events();
}

void pf::Frontend::wl_create_surface(wl_client *client,
                                     wl_resource *_surface)
{
	/// @todo Call the compositor to create the surface.
	auto surface = ph::g_server->compositor().lock()->create_surface();
	surface->set_wl_surface(_surface);
	surface->set_client(find_client(client));
	wl_resource_set_user_data(_surface, surface);
	m_surfaces.push_back(surface);
}

void pf::Frontend::xdg_shell_get_surface(wl_client *,
                                         wl_resource *_xdg_surface,
                                         wl_resource *_wl_surface)
{
	pfw::Surface *surface =
	        static_cast<pfw::Surface*>(wl_resource_get_user_data(_wl_surface));
	surface->set_xdg_surface(_xdg_surface);
	wl_resource_set_user_data(_xdg_surface, surface);
}

void pf::Frontend::xdg_surface_set_title(wl_client *,
                                         wl_resource *_surface,
                                         const char *title)
{
	pfw::Surface *surface =
	        static_cast<pfw::Surface*>(wl_resource_get_user_data(_surface));
	ph::g_server->compositor().lock()->set_surface_title(surface, std::string(title));
}

void pf::Frontend::wl_surface_attach(wayland::Surface *s, wl_resource *buffer)
{
	s->set_wl_buffer(buffer);
}

void pf::Frontend::wl_surface_commit(wayland::Surface *s)
{
	s->commit();
}

void pf::Frontend::wl_surface_frame(wayland::Surface *s, wl_resource *callback)
{
	s->set_wl_frame_callback(callback);
}

void pf::Frontend::wl_surface_set_input_region(wayland::Surface *s,
                                               pg::Region *region)
{
	s->set_input_region((region != nullptr) ? (*region) : pg::Region());
}

void pf::Frontend::wl_surface_set_opaque_region(wayland::Surface *s,
                                                pg::Region *region)
{
	s->set_opaque_region((region != nullptr) ? (*region) : pg::Region());
}

void pf::Frontend::wl_surface_set_buffer_scale(wayland::Surface *s, int scale)
{
	s->set_buffer_scale(scale);
}

#pragma clang diagnostic ignored "-Winvalid-offsetof"

void client_destroy_listener(wl_listener *listener, void*)
{
	pfw::Client *raw_client = nullptr;
	raw_client = wl_container_of(listener, raw_client, m_destroy_listener);
	ph::g_server->frontend().lock()->wl_client_destroyed(
	            raw_client->_wl_client());
}

void pf::Frontend::wl_client_destroyed(wl_client *raw_client)
{
	// Search for the client.
	auto client_pos = std::find_if(std::begin(m_clients), std::end(m_clients),
	                           [=] (std::shared_ptr<pfw::Client> c) {
		return (c->_wl_client() == raw_client);
	});

	if (client_pos == std::end(m_clients))
		throw std::runtime_error("Asked to destroy unregistered client.");

	std::shared_ptr<pfw::Client> client = *client_pos;

	// Remove every surface that the client owns.
	for (auto it = m_surfaces.begin(); it != m_surfaces.end(); it++) {
		if ((*it)->client().lock() == client) {
			ph::g_server->compositor().lock()->destroy_surface(*it);
		}
	}

	m_surfaces.remove_if([=](pfw::Surface *s) {
		if (s->client().lock() == client) {
			delete s;
			return true;
		}
		return false;
	});

	// Remove the client from the list.
	m_clients.erase(client_pos);
}

auto pf::Frontend::find_client(wl_client *wc) -> std::shared_ptr<pfw::Client>
{
	auto result = std::find_if(std::begin(m_clients), std::end(m_clients),
	                             [=] (std::shared_ptr<pfw::Client> c) {
		    return (c->_wl_client() == wc);
	});

	if (result == std::end(m_clients)) {
		// Create a new client.
		std::shared_ptr<pfw::Client> client = std::make_shared<pfw::Client>(wc);
		m_clients.push_back(client);

		// Add a destroy listener.
		client->wl_destroy_listener()->notify = client_destroy_listener;
		wl_client_add_destroy_listener(wc, client->wl_destroy_listener());
		return client;
	} else {
		return *result;
	}
}
